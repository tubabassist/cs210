package jdavis31;

import java.io.File;

/**
 * Provides file paths used to store database information.
 * 
 * @author Joshua Davis
 */
public interface FileStructure {
	public final String DIRECTORY = "jdavis31";
	public final String VARCHAR_FILE = DIRECTORY + File.separator + "varchars";
}
