package jdavis31.commands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jdavis31.OmniException;
import jdavis31.data.TableCollection;

/**
 * Checks syntax for correct define_index command and executes define_index
 * function.
 * 
 * @author Joshua Davis
 */
public class DefineIndexCommand implements ICommand {
	private Pattern pattern = Pattern.compile(
			"define\\s+index\\s+on\\s+(\\S+)\\s*(\\((\\S+)\\))\\s*;",
			Pattern.CASE_INSENSITIVE);
	private String tableName;
	private String fieldName;

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#matches(java.lang.String)
	 */
	@Override
	public boolean matches(String input) {
		Matcher matcher = pattern.matcher(input.trim());
		if (matcher.matches()) {
			tableName = matcher.group(1);
			fieldName = matcher.group(3);
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#execute()
	 */
	@Override
	public void execute() throws OmniException {
		TableCollection.getTC().defineIndex(tableName, fieldName);
		TableCollection.getTC().writeToFile();
	}
}