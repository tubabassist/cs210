package jdavis31.commands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jdavis31.OmniException;
import jdavis31.data.TableCollection;

/**
 * Checks syntax for correct drop command. Drops the table from the database and
 * commits database changes to file.
 * 
 * @author Joshua Davis
 */
public class DropCommand implements ICommand {
	private Pattern pattern = Pattern.compile("drop\\s+table\\s+(\\S+)\\s*;",
			Pattern.CASE_INSENSITIVE);
	private String tableName;

	@Override
	public boolean matches(String input) {
		Matcher matcher = pattern.matcher(input.trim());
		if (matcher.matches()) {
			tableName = matcher.group(1);
			return true;
		}
		return false;
	}

	@Override
	public void execute() throws OmniException {
		TableCollection.getTC().remove(tableName);
		TableCollection.getTC().writeToFile();
	}
}