package jdavis31.commands.queries;

import jdavis31.OmniException;
import jdavis31.commands.ICommand;
import jdavis31.data.Dataset;


public interface IQuery extends ICommand {
	
	Dataset executeQuery() throws OmniException;
}
