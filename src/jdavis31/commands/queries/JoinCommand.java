package jdavis31.commands.queries;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jdavis31.OmniException;
import jdavis31.data.Dataset;
import jdavis31.data.TableCollection;

/**
 * Checks syntax for correct join command and executes join function.
 * 
 * @author Joshua Davis
 */
public class JoinCommand implements IQuery {
	private Pattern pattern = Pattern.compile(
			"join\\s+(\\(.+\\)|\\S+)\\s+and\\s+(\\(.+\\)|\\S+)\\s*;",
			Pattern.CASE_INSENSITIVE);
	private String queryList1;
	private String queryList2;

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#matches(java.lang.String)
	 */
	@Override
	public boolean matches(String input) {
		Matcher matcher = pattern.matcher(input.trim());
		if (matcher.matches()) {
			queryList1 = matcher.group(1);
			queryList2 = matcher.group(2);
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#execute()
	 */
	@Override
	public void execute() throws OmniException {
		System.out.println(executeQuery().toString());
	}

	@Override
	public Dataset executeQuery() throws OmniException {
		return TableCollection.getTC().join(queryList1, queryList2);
	}
}