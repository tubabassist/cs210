package jdavis31.commands.queries;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jdavis31.OmniException;
import jdavis31.data.Dataset;
import jdavis31.data.TableCollection;

/**
 * Checks syntax for correct projection command and performs projection
 * function.
 * 
 * @author Joshua Davis
 */
public class ProjectionCommand implements IQuery {
	private Pattern pattern = Pattern.compile(
			"project\\s+(\\(.+\\)|\\S+)\\s+over\\s+(.+)\\s*;",
			Pattern.CASE_INSENSITIVE);
	private String queryList;
	private String fieldList;

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#matches(java.lang.String)
	 */
	@Override
	public boolean matches(String input) {
		Matcher matcher = pattern.matcher(input.trim());
		if (matcher.matches()) {
			queryList = matcher.group(1);
			fieldList = matcher.group(2);
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#execute()
	 */
	@Override
	public void execute() throws OmniException {
		System.out.println(executeQuery().toString());
	}

	@Override
	public Dataset executeQuery() throws OmniException {
		return TableCollection.getTC().project(queryList, fieldList);
	}
}