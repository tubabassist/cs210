package jdavis31.commands.queries;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jdavis31.OmniException;
import jdavis31.data.Dataset;
import jdavis31.data.TableCollection;

/**
 * Checks syntax for correct order command and executes order function.
 * 
 * @author Joshua Davis
 */
public class OrderCommand implements IQuery {
	private Pattern pattern = Pattern
			.compile(
					"order\\s+(\\(.+\\)|\\S+)\\s+by\\s+(\\S+)\\s*(?:(decending))?\\s*;",
					Pattern.CASE_INSENSITIVE);
	private String queryList;
	private String fieldName;
	private boolean decending;

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#matches(java.lang.String)
	 */
	@Override
	public boolean matches(String input) {
		Matcher matcher = pattern.matcher(input.trim());
		if (matcher.matches()) {
			queryList = matcher.group(1);
			fieldName = matcher.group(2);
			if (matcher.group(3) != null)
				decending = true;
			else
				decending = false;
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#execute()
	 */
	@Override
	public void execute() throws OmniException {
		System.out.println(executeQuery().toString());
	}

	@Override
	public Dataset executeQuery() throws OmniException {
		return TableCollection.getTC().order(queryList, fieldName, decending);
	}
}