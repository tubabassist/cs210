package jdavis31.commands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jdavis31.OmniException;
import jdavis31.data.TableCollection;

/**
 * Checks syntax for correct delete command and removes rows of data from a
 * table.
 * 
 * @author Joshua Davis
 */
public class DeleteCommand implements ICommand {
	private Pattern pattern = Pattern
			.compile("delete\\s+(\\S+)(?:\\s+where(.+))?\\s*;",
					Pattern.CASE_INSENSITIVE);
	private String tableName;
	private String whereClause;

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#matches(java.lang.String)
	 */
	@Override
	public boolean matches(String input) {
		Matcher matcher = pattern.matcher(input.trim());
		if (matcher.matches()) {
			tableName = matcher.group(1);
			whereClause = matcher.group(2);
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#execute()
	 */
	@Override
	public void execute() throws OmniException {
		TableCollection.getTC().delete(tableName, whereClause);
	}
}