package jdavis31.commands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jdavis31.OmniException;
import jdavis31.data.TableCollection;

/**
 * Checks syntax for correct rename command. Renames the table and commits
 * database changes to file.
 * 
 * @author Joshua Davis
 */
public class RenameCommand implements ICommand {
	private Pattern pattern = Pattern.compile(
			"rename\\s+table\\s+(\\S+)\\s+to\\s+(\\S+)\\s*;",
			Pattern.CASE_INSENSITIVE);
	private String oldName;
	private String newName;

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#matches(java.lang.String)
	 */
	@Override
	public boolean matches(String input) {
		Matcher matcher = pattern.matcher(input.trim());
		if (matcher.matches()) {
			oldName = matcher.group(1);
			newName = matcher.group(2);
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#execute()
	 */
	@Override
	public void execute() throws OmniException {
		TableCollection.getTC().rename(oldName, newName);
		TableCollection.getTC().writeToFile();
	}
}