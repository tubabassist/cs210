package jdavis31.commands;

import jdavis31.OmniException;

/**
 * Defines the requirements for classes that contain the syntax and function of
 * commands.
 * 
 * @author Joshua Davis
 */
public interface ICommand {

	/**
	 * Checks input string for correct command syntax.
	 *
	 * @param input
	 *            Command to be checked against a regular expression that
	 *            defines correct command syntax.
	 * @return True if the command is syntactically correct. False if command is
	 *         not correct.
	 */
	boolean matches(String input);

	/**
	 * Primary function of a specific command class. Usually called when
	 * matches(String input) returns true for a given input.
	 * @throws OmniException Passes through exceptions from any methods called in the execute() method.
	 */
	void execute() throws OmniException;
}