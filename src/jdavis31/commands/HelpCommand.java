package jdavis31.commands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jdavis31.utilities.Utilities;

/**
 * Checks syntax for correct exit command and exits the database program.
 * 
 * @author Joshua Davis
 */
public class HelpCommand implements ICommand {
	private Pattern pattern = Pattern.compile("help\\s*;",
			Pattern.CASE_INSENSITIVE);

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#matches(java.lang.String)
	 */
	@Override
	public boolean matches(String input) {
		Matcher matcher = pattern.matcher(input.trim());
		return matcher.matches();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#execute()
	 */
	@Override
	public void execute() {
		Utilities.printHelp();
	}
}