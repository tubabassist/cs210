package jdavis31.commands;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jdavis31.OmniException;
import jdavis31.data.Table;
import jdavis31.data.TableCollection;

public class ReadTableCommand implements ICommand {
	private Pattern pattern = Pattern.compile("define\\s+table\\s+(\\S+)\\s+read\\s+'(.+)'\\s*;",
			Pattern.CASE_INSENSITIVE);
	private String tableName;
	private String filepath;
	
	@Override
	public boolean matches(String input) {
		Matcher matcher = pattern.matcher(input.trim());
		if (matcher.matches()) {
			tableName = matcher.group(1);
			filepath = matcher.group(2);
			return true;
		}
		return false;
	}
	
	@Override
	public void execute() throws OmniException {
		try {
			Scanner inputFile = new Scanner(new File(filepath));
			TableCollection.getTC().put(new Table(tableName, inputFile.nextLine()));
			TableCollection.getTC().writeToFile();
			System.out.println("Beginning read operation. Please wait.");
			while(inputFile.hasNextLine()) {
				TableCollection.getTC().insert(tableName, inputFile.nextLine());
			}
			System.out.println("Read operation complete.");
			inputFile.close();
		} catch (FileNotFoundException e) {
			throw new OmniException("File '" + filepath + "' was not found.");
		}
	}
}