package jdavis31.commands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jdavis31.OmniException;
import jdavis31.data.TableCollection;

/**
 * Checks syntax for correct update command and updates rows of data in a table.
 * 
 * @author Joshua Davis
 */
public class UpdateCommand implements ICommand {
	private Pattern pattern = Pattern
			.compile(
					"update\\s+(\\S+)\\s+set\\s+(\\S+)\\s*=\\s*(\\S+)(?:\\s+where(.+))?\\s*;",
					Pattern.CASE_INSENSITIVE);
	private String tableName;
	private String fieldName;
	private String value;
	private String whereClause;

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#matches(java.lang.String)
	 */
	@Override
	public boolean matches(String input) {
		Matcher matcher = pattern.matcher(input.trim());
		if (matcher.matches()) {
			tableName = matcher.group(1);
			fieldName = matcher.group(2);
			value = matcher.group(3);
			whereClause = matcher.group(4);
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#execute()
	 */
	@Override
	public void execute() throws OmniException {
		TableCollection.getTC()
				.update(tableName, fieldName, value, whereClause);
	}
}