package jdavis31.commands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jdavis31.OmniException;
import jdavis31.data.TableCollection;

/**
 * Checks syntax for correct insert command and adds a new row of data to a
 * table.
 * 
 * @author Joshua Davis
 */
public class InsertCommand implements ICommand {
	private Pattern pattern = Pattern.compile(
			"insert\\s*(\\((.+)\\))\\s*into\\s+(\\S+)\\s*;",
			Pattern.CASE_INSENSITIVE);
	private String valueList;
	private String tableName;

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#matches(java.lang.String)
	 */
	@Override
	public boolean matches(String input) {
		Matcher matcher = pattern.matcher(input.trim());
		if (matcher.matches()) {
			valueList = matcher.group(2);
			tableName = matcher.group(3);
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#execute()
	 */
	@Override
	public void execute() throws OmniException {
		TableCollection.getTC().insert(tableName, valueList);
	}
}