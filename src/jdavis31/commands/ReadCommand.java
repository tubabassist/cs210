package jdavis31.commands;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jdavis31.Driver;
import jdavis31.OmniException;

/**
 * Checks syntax for correct read command and reads the contents of a text file as console input.
 * 
 * @author Joshua Davis
 */
public class ReadCommand implements ICommand {
	private Pattern pattern = Pattern.compile("read\\s+'(.+)'\\s*;",
			Pattern.CASE_INSENSITIVE);
	private String filepath;

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#matches(java.lang.String)
	 */
	@Override
	public boolean matches(String input) {
		Matcher matcher = pattern.matcher(input.trim());
		if (matcher.matches()) {
			filepath = matcher.group(1);
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#execute()
	 */
	@Override
	public void execute() throws OmniException {
		try {
			Scanner inputFile = new Scanner(new File(filepath));
			new Driver().run(inputFile);
			inputFile.close();
		} catch (FileNotFoundException e) {
			throw new OmniException("File '" + filepath + "' was not found.");
		}
	}
}