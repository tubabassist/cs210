package jdavis31.commands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jdavis31.OmniException;
import jdavis31.data.Table;
import jdavis31.data.TableCollection;

/**
 * Checks syntax for correct define_table command. Creates a new table with the
 * input parameters and commits the database to file.
 * 
 * @author Joshua Davis
 */
public class DefineTableCommand implements ICommand {
	private Pattern pattern = Pattern
			.compile(
					"define\\s+table\\s+(\\S+)\\s+having\\s+fields\\s*(\\((.+)\\))\\s*;",
					Pattern.CASE_INSENSITIVE);
	private String tableName;
	private String extendedFieldList;

	@Override
	public boolean matches(String input) {
		Matcher matcher = pattern.matcher(input.trim());
		if (matcher.matches()) {
			tableName = matcher.group(1);
			extendedFieldList = matcher.group(3);
			return true;
		}
		return false;
	}

	@Override
	public void execute() throws OmniException {
		TableCollection.getTC().put(new Table(tableName, extendedFieldList));
		TableCollection.getTC().writeToFile();
	}
}