package jdavis31.commands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jdavis31.OmniException;
import jdavis31.data.TableCollection;

/**
 * Checks syntax for correct print command and prints the name and fields of the
 * input table. If "dictionary" is passed through print command, the names and
 * fields of all tables in the database will be printed.
 * 
 * @author Joshua Davis
 */
public class PrintCommand implements ICommand {
	private Pattern pattern = Pattern.compile("print\\s+(\\S+)\\s*;",
			Pattern.CASE_INSENSITIVE);
	private String tableName;

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#matches(java.lang.String)
	 */
	@Override
	public boolean matches(String input) {
		Matcher matcher = pattern.matcher(input.trim());
		if (matcher.matches()) {
			tableName = matcher.group(1);
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see one.commands.ICommand#execute()
	 */
	@Override
	public void execute() throws OmniException {
		if (tableName.trim().equalsIgnoreCase("DICTIONARY")) {
			System.out.println(TableCollection.getTC().toString());
		} else {
			System.out.println(TableCollection.getTC().selectTable(tableName, null));
		}
	}
}