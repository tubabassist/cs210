package jdavis31.data;

import java.io.IOException;
import java.io.RandomAccessFile;

import jdavis31.Driver;

public class BinaryNode {

	protected long location;
	private static final short LEFT_CHILD_RELATIVE_LOCATION = 8;
	private static final short RIGHT_CHILD_RELATIVE_LOCATION = 16;
	private static final short PARENT_RELATIVE_LOCATION = 24;

	public BinaryNode(long location, RandomAccessFile indexFile) {
		try {
			this.location = location;
			if (location == indexFile.length()) {
				indexFile.seek(indexFile.length());
				for (int i = 0; i < 4; i++) {
					indexFile.writeLong(-1);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public BinaryNode(BinaryNode node, RandomAccessFile indexFile) {
		try {
			this.location = indexFile.length();
			setRow(node.getRow(indexFile), indexFile);
			setLeftChild(node.getLeftChild(indexFile), indexFile);
			setRightChild(node.getRightChild(indexFile), indexFile);
			setParent(node.getParent(indexFile), indexFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void delete(RandomAccessFile indexFile) throws IOException {
		BinaryNode parent = getParent(indexFile);
		long leftChildPos = parent.getLeftChild(indexFile).getRow(indexFile);
		long childPos = getRow(indexFile);
		
		if (leftChildPos == childPos) {
			if (getLeftChild(indexFile).getRow(indexFile) != -1) {
				indexFile.seek(parent.location + LEFT_CHILD_RELATIVE_LOCATION);
				indexFile.writeLong(getLeftChild(indexFile).location);
			} else if (getRightChild(indexFile).getRow(indexFile) != -1) {
				indexFile.seek(parent.location + LEFT_CHILD_RELATIVE_LOCATION);
				indexFile.writeLong(getRightChild(indexFile).location);
			} else {
				indexFile.seek(parent.location + LEFT_CHILD_RELATIVE_LOCATION);
				indexFile.writeLong(-1);
			}
		} else {
			if (getLeftChild(indexFile).getRow(indexFile) != -1) {
				indexFile.seek(parent.location + RIGHT_CHILD_RELATIVE_LOCATION);
				indexFile.writeLong(getLeftChild(indexFile).location);
			} else if (getRightChild(indexFile).getRow(indexFile) != -1) {
				indexFile.seek(parent.location + RIGHT_CHILD_RELATIVE_LOCATION);
				indexFile.writeLong(getRightChild(indexFile).location);
			} else {
				indexFile.seek(parent.location + RIGHT_CHILD_RELATIVE_LOCATION);
				indexFile.writeLong(-1);
			}
		}
	}

	public BinaryNode getLeftChild(RandomAccessFile indexFile) {
		BinaryNode leftChild = null;
		try {
			indexFile.seek(location + LEFT_CHILD_RELATIVE_LOCATION);
			leftChild = new BinaryNode(indexFile.readLong(), indexFile);
			if (leftChild.location == -1) {
				leftChild = new BinaryNode(indexFile.length(), indexFile);
				this.setLeftChild(leftChild, indexFile);
				leftChild.setParent(this, indexFile);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return leftChild;
	}

	public BinaryNode getParent(RandomAccessFile indexFile) {
		BinaryNode parent = null;
		try {
			indexFile.seek(location + PARENT_RELATIVE_LOCATION);
			parent = new BinaryNode(indexFile.readLong(), indexFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return parent;
	}

	public BinaryNode getRightChild(RandomAccessFile indexFile) {
		BinaryNode rightChild = null;
		try {
			indexFile.seek(location + RIGHT_CHILD_RELATIVE_LOCATION);
			rightChild = new BinaryNode(indexFile.readLong(), indexFile);
			if (rightChild.location == -1) {
				rightChild = new BinaryNode(indexFile.length(), indexFile);
				this.setRightChild(rightChild, indexFile);
				rightChild.setParent(this, indexFile);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return rightChild;
	}

	public Long getRow(RandomAccessFile indexFile) {
		try {
			indexFile.seek(location);
			return indexFile.readLong();
		} catch (IOException e) {
			return null;
		}
	}

	public BinaryNode setLeftChild(BinaryNode leftChild,
			RandomAccessFile indexFile) throws IOException {
		indexFile.seek(location + LEFT_CHILD_RELATIVE_LOCATION);
		indexFile.writeLong(leftChild.location);
		leftChild.setParent(this, indexFile);
		return leftChild;
	}

	public BinaryNode setParent(BinaryNode parent, RandomAccessFile indexFile)
			throws IOException {
		indexFile.seek(location + PARENT_RELATIVE_LOCATION);
		indexFile.writeLong(parent.location);
		return parent;
	}

	public BinaryNode setRightChild(BinaryNode rightChild,
			RandomAccessFile indexFile) throws IOException {
		indexFile.seek(location + RIGHT_CHILD_RELATIVE_LOCATION);
		indexFile.writeLong(rightChild.location);
		rightChild.setParent(this, indexFile);
		return rightChild;
	}

	public void setRow(Long rowPos, RandomAccessFile indexFile)
			throws IOException {
		indexFile.seek(location);
		indexFile.writeLong(rowPos);
	}

	public String toString(RandomAccessFile indexFile) {
		try {
			indexFile.seek(location);
			return "{ " + getRow(indexFile) + ", "
					+ getLeftChild(indexFile).getRow(indexFile) + ", "
					+ getRightChild(indexFile).getRow(indexFile) + ", "
					+ getParent(indexFile).getRow(indexFile) + " }";
		} catch (IOException e) {
			if (Driver.debug) {
				e.printStackTrace();
				System.out.println("DEBUG = " + Driver.debug
						+ ", stack trace printed.");
			}
			return "Error encountered during BinaryNode toString().";
		}
	}
}
