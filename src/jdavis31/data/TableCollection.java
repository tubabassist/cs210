package jdavis31.data;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jdavis31.Driver;
import jdavis31.FileStructure;
import jdavis31.OmniException;
import jdavis31.utilities.Utilities;

/**
 * The main database object that provides information about and access to all
 * tables.
 * 
 * @author Joshua Davis
 */
public class TableCollection implements FileStructure, Serializable {

	private static final long serialVersionUID = 1L;
	private static TableCollection tc;
	private HashMap<String, Table> tables = new HashMap<String, Table>();

	private TableCollection() {
	}

	/**
	 * Adds a field to a table in the TableCollection.
	 * 
	 * @param tableName
	 *            Name of the table that the field is being added to.
	 * @param fieldName
	 *            Name of the field to be added.
	 * @param fieldType
	 *            Type of the field to be added.
	 * @throws OmniException
	 *             Thrown if the field name already exists in the table.
	 */
	public void addField(String tableName, String fieldName, String fieldType)
			throws OmniException {
		tables.get(tableName.toUpperCase()).add(fieldName, fieldType);
	}

	public void backup(String filepath) throws OmniException {
		try {
			for (Table table : tables.values()) {
				table.getData();
			}
			FileOutputStream fileOut = new FileOutputStream(filepath);
			ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
			objectOut.writeObject(this);
			objectOut.close();
			fileOut.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new OmniException(
					"There was an error creating the backup file!  "
							+ "You may not have appropriate read/write permissions in this location.");
		}
	}

	/**
	 * Provides access to the TableCollection object so that TableCollection
	 * methods may be be called. If no TableCollection exists, a new one is
	 * constructed.
	 * 
	 * @return The TableCollection object.
	 */
	public static TableCollection getTC() {
		if (tc == null)
			tc = new TableCollection();
		return tc;
	}

	public void defineExistingIndex(String tableName, String fieldName)
			throws OmniException {
		getTable(tableName).defineExistingIndex(fieldName);
	}

	public void defineIndex(String tableName, String fieldName)
			throws OmniException {
		getTable(tableName).defineIndex(fieldName);
	}

	/**
	 * Deletes rows of data from a table.
	 * 
	 * @param tableName
	 *            Name of the table to have a row deleted.
	 * @param whereClause
	 *            Rows are deleted when this where clause evaluates true.
	 * @throws OmniException
	 *             Thrown if a table is not defined, a field is not defined, or
	 *             if there is an error reading or writing to the table files.
	 */
	public void delete(String tableName, String whereClause)
			throws OmniException {
		getTable(tableName).delete(whereClause);
	}

	/**
	 * Inserts a row of data into a table.
	 * 
	 * @param tableName
	 *            Name of the table to insert data into.
	 * @param valueList
	 *            Row of data to be inserted.
	 * @throws OmniException
	 *             Thrown if the table name is not defined.
	 */
	public void insert(String tableName, String valueList) throws OmniException {
		getTable(tableName).insert(valueList);
	}

	public Dataset intersect(String queryList1, String queryList2)
			throws OmniException {
		return processQuery(queryList1).intersect(processQuery(queryList2));
	}

	public Dataset join(String queryList1, String queryList2)
			throws OmniException {
		return processQuery(queryList1).join(processQuery(queryList2));
	}

	public Dataset minus(String queryList1, String queryList2)
			throws OmniException {
		return processQuery(queryList1).minus(processQuery(queryList2));
	}

	public Dataset order(String queryList, String fieldName, boolean decending)
			throws OmniException {
		return processQuery(queryList).orderBy(fieldName, decending);
	}

	public Dataset project(String queryList, String fieldList)
			throws OmniException {
		return processQuery(queryList).project(fieldList);
	}

	/**
	 * Checks to see if a table name already exists, then inserts a new table
	 * into the TableCollection.
	 * 
	 * @param table
	 *            Table to be put into the TableCollection.
	 * @throws OmniException
	 *             Thrown if a table already exists with the new table's name.
	 */
	public void put(Table table) throws OmniException {
		if (tables.containsKey(table.getName())) {
			throw new OmniException("There is already a table named '"
					+ table.getName() + "'!");
		}
		tables.put(table.getName(), table);
	}

	/**
	 * Removes a table from the TableCollection.
	 * 
	 * @param tableName
	 *            Name of the table to be removed.
	 * @throws OmniException
	 *             An exception thrown if the table is not found.
	 */
	public void remove(String tableName) throws OmniException {
		try {
			getTable(tableName).deleteBin();
			tables.remove(tableName.toUpperCase());
		} catch (IOException e) {
			if (Driver.debug) {
				e.printStackTrace();
				System.out.println("DEBUG = " + Driver.debug
						+ ", stack trace printed.");
			}
			throw new OmniException("There was an error dropping table '"
					+ tableName + "'!");
		}
	}

	/**
	 * Checks to see if the new name already exists in the TableCollection, then
	 * renames a table in the TableCollection.
	 * 
	 * @param oldName
	 *            Name of the table to be renamed.
	 * @param newName
	 *            New name for the table.
	 * @throws OmniException
	 *             Thrown if the new name already exists in the database.
	 */
	public void rename(String oldName, String newName) throws OmniException {
		newName = newName.toUpperCase();
		if (tables.containsKey(newName)) {
			throw new OmniException("There is already a table named '"
					+ newName + "'!");
		}
		getTable(oldName).setName(newName);
		tables.put(newName, tables.remove(oldName.toUpperCase()));
	}

	public void restore(String filepath) throws OmniException {
		try {
			FileInputStream fileIn = new FileInputStream(filepath);
			ObjectInputStream objectIn = new ObjectInputStream(fileIn);
			TableCollection.tc = (TableCollection) objectIn.readObject();
			objectIn.close();
			fileIn.close();
			for (Table table : tc.tables.values()) {
				table.writeBin();
			}
			tc.writeToFile();
		} catch (IOException | ClassNotFoundException e) {
			throw new OmniException(
					"There was an error restoring from this file!");
		}
	}

	/**
	 * Selects rows of data from a table.
	 * 
	 * @param tableName
	 *            Name of the table.
	 * @param whereClause
	 *            Rows are selected when this where clause evaluates true.
	 * @return A string representation of all selected rows.
	 * @throws OmniException
	 *             Thrown if a table is not defined, a field is not defined, or
	 *             if there is an error reading or writing to the table files.
	 */
	public Dataset selectQuery(String queryList, String whereClause)
			throws OmniException {
		return processQuery(queryList).select(whereClause);
	}

	public Dataset selectTable(String tableName, String whereClause)
			throws OmniException {
		if (tableName.matches("\\s*\\((.*)\\)\\s*"))
			return selectQuery(tableName, whereClause);
		else
			return getTable(tableName).select(whereClause);
	}

	/**
	 * Provides each table in the TableCollection and lists every field for each
	 * table.
	 * 
	 * @return The TableCollection represented as a string.
	 */
	public String toString() {
		String tableCollection = "";

		for (String tableName : tables.keySet()) {
			tableCollection += "\n" + tables.get(tableName).toString();
		}
		tableCollection += "\n<--End of Dictionary-->\n";

		return tableCollection;
	}

	public Dataset union(String queryList1, String queryList2)
			throws OmniException {
		return processQuery(queryList1).union(processQuery(queryList2));
	}

	/**
	 * Updates field data in a table.
	 * 
	 * @param tableName
	 *            Name of the table.
	 * @param fieldName
	 *            Name of the field to be updated.
	 * @param value
	 *            New value to place in the field.
	 * @param whereClause
	 *            Fields are updated when this where clause evaluates true.
	 * @throws OmniException
	 *             Thrown if a table is not defined, a field is not defined, or
	 *             if there is an error reading or writing to the table files.
	 */
	public void update(String tableName, String fieldName, String value,
			String whereClause) throws OmniException {
		getTable(tableName).update(fieldName, value, whereClause);
	}

	/**
	 * Commits the TableCollection to the database file "database.xml".
	 * 
	 * @throws OmniException
	 *             Thrown if the file exists but is a directory rather than a
	 *             regular file, does not exist but cannot be created, or cannot
	 *             be opened for any other reason.
	 */
	public void writeToFile() throws OmniException {
		String ENCODING = "ISO-8859-1";
		try {
			PrintWriter out = new PrintWriter(new FileOutputStream(DIRECTORY
					+ "/database.xml"));
			out.println("<?xml version=\"1.0\" encoding=\"" + ENCODING + "\"?>");
			out.println("<!DOCTYPE USERS SYSTEM \"database.dtd\">");
			out.println("<TABLE_COLLECTION>");
			for (String tableName : tables.keySet()) {
				out.println(tables.get(tableName).toXMLString());
			}
			out.println("</TABLE_COLLECTION>");
			out.close();
		} catch (FileNotFoundException e) {
			throw new OmniException(
					"There was an error opening or creating required files!  "
							+ "You may not have appropriate read/write permissions in this location.");
		}
	}

	private Table getTable(String tableName) throws OmniException {
		Table table = tables.get(tableName.toUpperCase());
		if (table == null)
			throw new OmniException("Table '" + tableName.toUpperCase()
					+ "' is not defined!");
		return table;
	}

	private Dataset processQuery(String queryList) throws OmniException {
		Pattern pattern = Pattern.compile("\\s*\\((.*)\\)\\s*");
		Matcher matcher = pattern.matcher(queryList);
		if (matcher.matches()) {
			return Utilities.processQueryInput(matcher.group(1) + ";");
		} else {
			return getTable(queryList).getData();
		}
	}
}