package jdavis31.data;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jdavis31.OmniException;
import jdavis31.data.fields.Field;
import jdavis31.data.values.Value;

public class WhereStatement {
	private final boolean alwaysTrue;
	private String relop;
	@SuppressWarnings("rawtypes")
	protected Value value;
	private Field field;
	private int fieldPos = -1;

	public WhereStatement(String whereClause, ArrayList<Field> fields)
			throws OmniException {

		if (whereClause == null) {
			alwaysTrue = true;
			return;
		} else
			alwaysTrue = false;

		String fieldName = null;
		String newValue = null;
		String relops = "=|<|>|<=|>=|!=";
		Pattern pattern = Pattern.compile("(\\S*(?!" + relops + ").)\\s*("
				+ relops + ")\\s*((?!" + relops + ").+)");
		Matcher matcher = pattern.matcher(whereClause.trim());
		if (matcher.matches()) {
			fieldName = matcher.group(1).trim().toLowerCase();
			relop = matcher.group(2);
			newValue = matcher.group(3);
		} else {
			throw new OmniException(
					"Invalid statement!  Check syntax in where clause.");
		}

		for (int i = 0; i < fields.size(); i++) {
			if (fields.get(i).getName().equals(fieldName)) {
				this.field = fields.get(i);
				fieldPos = i;
				break;
			}
		}
		if (fieldPos == -1)
			throw new OmniException("Field '" + fieldName
					+ "' is not defined for this table!");
		this.value = field.makeValue(newValue);
	}

	public boolean evaluate(RandomAccessFile binFile, long rowPos)
			throws IOException, OmniException {
		if (alwaysTrue)
			return true;
		binFile.seek(rowPos + this.field.getPos());
		return evaluate(this.field.read(binFile));
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean evaluate(Value value) throws OmniException {
		if (alwaysTrue)
			return true;

		boolean eval = false;
		switch (relop) {
		case "=":
			eval = value.compareTo(this.value) == 0;
			break;
		case "!=":
			eval = value.compareTo(this.value) != 0;
			break;
		case "<":
			eval = value.compareTo(this.value) < 0;
			break;
		case ">":
			eval = value.compareTo(this.value) > 0;
			break;
		case "<=":
			eval = value.compareTo(this.value) <= 0;
			break;
		case ">=":
			eval = value.compareTo(this.value) >= 0;
			break;
		}
		return eval;
	}

	public boolean evaluate(Row row) throws OmniException {
		if (alwaysTrue)
			return true;
		return evaluate(row.values.get(fieldPos));
	}

	public Iterator<Long> iterator(final RandomAccessFile tableData,
			final int rowSize) throws OmniException, IOException {

		if (field != null && field.hasIndex()) {
			return iterateIndex(tableData, rowSize);
		}

		return new Iterator<Long>() {
			long rowPos = 0 - rowSize;

			@Override
			public boolean hasNext() {
				try {
					rowPos += rowSize;
					if (rowPos >= tableData.length())
						return false;
					tableData.seek(rowPos);
					if (!tableData.readBoolean()
							|| !evaluate(tableData, rowPos))
						return hasNext();
					else
						return true;
				} catch (IOException | OmniException e) {
					e.printStackTrace();
					return false;
				}
			}

			@Override
			public Long next() {
				return rowPos;
			}

			@Override
			public void remove() {
				System.err.println("Unimplemented method called.");
			}

		};
	}

	public Iterator<Row> iterator(final ArrayList<Row> rows) {
		return new Iterator<Row>() {

			int rowPos = -1;
			Row row;

			@Override
			public boolean hasNext() {
				try {
					rowPos++;
					if (rowPos >= rows.size())
						return false;
					row = rows.get(rowPos);
					if (!evaluate(row))
						return hasNext();
					else
						return true;
				} catch (OmniException e) {
					e.printStackTrace();
					return false;
				}
			}

			@Override
			public Row next() {
				return row;
			}

			@Override
			public void remove() {
				System.err.println("Unimplemented method called.");
			}

		};
	}

	public Iterator<Long> iterateIndex(final RandomAccessFile tableData,
			final int rowSize) throws IOException, OmniException {
		List<Long> positions = null;
		BinarySearchTree index = field.getIndex();
		BinaryNode node = null;

		switch (relop) {
		case "=":
			positions = new ArrayList<Long>();
			try {
				node = index.select(value, index.getRoot(), tableData, false);
				positions.add(node.getRow(index.indexFile));
			} catch (OmniException e) {
			}
			break;
		case "!=":
			positions = index.inOrderWalk(index.getRoot());
			try {
				node = index.select(value, index.getRoot(), tableData, false);
				positions.remove(node.getRow(index.indexFile));
			} catch (OmniException e) {
			}
			break;
		case "<":
			node = index.select(value, index.getRoot(), tableData, true);
			positions = index.inOrderWalk(index.getRoot());
			positions = positions.subList(0,
					positions.indexOf(node.getRow(index.indexFile)));
			break;
		case ">":
			node = index.select(value, index.getRoot(), tableData, true);
			positions = index.inOrderWalk(index.getRoot());
			positions = positions.subList(
					positions.indexOf(node.getRow(index.indexFile)),
					positions.size());
			break;
		case "<=":
			node = index.select(value, index.getRoot(), tableData, true);
			positions = index.inOrderWalk(index.getRoot());
			positions = positions.subList(0,
					positions.indexOf(node.getRow(index.indexFile)) + 1);
			break;
		case ">=":
			node = index.select(value, index.getRoot(), tableData, true);
			positions = index.inOrderWalk(index.getRoot());
			positions = positions.subList(
					positions.indexOf(node.getRow(index.indexFile)) + 1,
					positions.size());
			break;
		}
		return positions.iterator();
	}
}
