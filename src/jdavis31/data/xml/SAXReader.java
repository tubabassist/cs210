package jdavis31.data.xml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Scanner;

import jdavis31.FileStructure;
import jdavis31.OmniException;
import jdavis31.data.Table;
import jdavis31.data.TableCollection;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * Restores a database from an XML file. If the XML file does not exist, a new
 * one is created with the current TableCollection information. The file
 * "database.dtd" is also required. If "database.dtd" does not exist, a new one
 * is created.
 * 
 * @author Joshua Davis
 */
public class SAXReader implements FileStructure {

	/**
	 * Constructor for SAXReader. Creates an XML reader and reads in database
	 * information. Writes XML and DTD files as necessary.
	 *
	 * @param filename
	 *            File path of the XML file storing the database to be restored.
	 * @throws SAXException
	 *             If no default XMLReader class can be identified and
	 *             instantiated, or any SAX exception, possibly wrapping another
	 *             exception.
	 * @throws IOException
	 *             An IO exception from the parser, possibly from a byte stream
	 *             or character stream supplied by the application.
	 * @throws OmniException
	 *             Thrown if the file exists but is a directory rather than a
	 *             regular file, does not exist but cannot be created, or cannot
	 *             be opened for any other reason.
	 */
	public void saxReader(String filename) throws SAXException, IOException,
			OmniException {
		File xmlFile = new File(filename);
		if (xmlFile.createNewFile()) {
			TableCollection.getTC().writeToFile();
			return;
		}
		File dtdFile = new File(DIRECTORY + "/database.dtd");
		if (dtdFile.createNewFile()) {
			writeDTD(dtdFile);
		}
		XMLReader reader = XMLReaderFactory.createXMLReader();
		reader.setContentHandler(new ContentHandler());
		reader.parse(xmlFile.getCanonicalPath());
	}

	private void writeDTD(File dtdFile) throws OmniException,
			FileNotFoundException {
		InputStream input = this.getClass().getResourceAsStream("database.dtd");
		Scanner sc = new Scanner(input);
		PrintWriter out = new PrintWriter(dtdFile);
		while (sc.hasNextLine()) {
			out.println(sc.nextLine());
		}
		sc.close();
		out.close();
	}

	private class ContentHandler extends DefaultHandler {
		private String tableName;
		private String fieldName;
		private String type;
		private String length;
		private boolean index;

		@Override
		public void startElement(String uri, String localName, String qName,
				Attributes attributes) throws SAXException {
			if (qName.equalsIgnoreCase("FIELD")) {
				fieldName = attributes.getValue("NAME");
				type = attributes.getValue("TYPE");
				if (type.equalsIgnoreCase("char")) {
					length = attributes.getValue("LENGTH");
				}
				index = attributes.getValue("INDEX").equalsIgnoreCase("TRUE");
			} else if (qName.equalsIgnoreCase("TABLE")) {
				try {
					tableName = attributes.getValue("NAME");
					TableCollection.getTC().put(new Table(tableName));
				} catch (OmniException e) {
					throw new SAXException(e.getMessage());
				}
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName)
				throws SAXException {
			if (qName.equalsIgnoreCase("FIELD")) {
				if (type.equalsIgnoreCase(("char"))) {
					type += "(" + length + ")";
				}
				try {
					TableCollection.getTC()
							.addField(tableName, fieldName, type);
					if (index)
						TableCollection.getTC().defineExistingIndex(tableName,
								fieldName);
				} catch (OmniException e) {
					throw new SAXException(e.getMessage());
				}
				fieldName = "";
			}
		}
	}
}