package jdavis31.data.xml;

import java.util.HashMap;

/**
 * Stores coding information and encodes some(ie 6) symbols that just don't work
 * in entities.
 * 
 * @author Ray Morehead
 */
public class XMLEncoder {
	private static HashMap<String, String> codes;

	static {
		codes = new HashMap<String, String>();
		codes.put("\"", "&quot;");
		codes.put("&", "&amp;");
		codes.put("'", "&apos;");
		codes.put("<", "&lt;");
		codes.put(">", "&gt;");
	}

	private XMLEncoder() {
	}

	/**
	 * Encodes some(ie 6) symbols that just don't work in entities.
	 *
	 * @param input
	 *            String to be encoded.
	 * @return Encoded string.
	 */
	public static String encode(String input) {
		for (String x : codes.keySet()) {
			input = input.replace(x, codes.get(x));
		}
		return input;
	}
}