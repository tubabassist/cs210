package jdavis31.data.fields;

import java.io.IOException;
import java.io.RandomAccessFile;

import jdavis31.OmniException;
import jdavis31.data.values.BooleanValue;

/**
 * A BooleanField provides information about, as well as access to, a single
 * boolean field of a table.
 * 
 * @author Joshua Davis
 */
public class BooleanField extends Field {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for BooleanField. Simply passes fieldName to constructor of
	 * Field.
	 *
	 * @param fieldName
	 *            Name for the booleanField to be created.
	 */
	public BooleanField(String fieldName, int position) {
		super(fieldName, position);
		byteSize = 1;
	}

	public BooleanValue makeValue(String value) throws OmniException {
		return new BooleanValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jdavis31.data.fields.Field#read(java.io.RandomAccessFile)
	 */
	@Override
	public BooleanValue read(RandomAccessFile binFile) throws IOException {
		return new BooleanValue(binFile);
	}

	@Override
	public BooleanField rename(String newName) {
		return new BooleanField(newName, this.position);
	}

	@Override
	public void write(RandomAccessFile binFile, String value)
			throws IOException, OmniException {
		BooleanValue newValue = new BooleanValue(value);
		newValue.write(binFile);
	}
}