package jdavis31.data.fields;

import java.io.IOException;
import java.io.RandomAccessFile;

import jdavis31.OmniException;
import jdavis31.data.values.CharValue;
import jdavis31.utilities.Utilities;

/**
 * A CharField provides information about, as well as access to, a single
 * fixed-length character field of a Table.
 * 
 * @author Joshua Davis
 */
public class CharField extends Field {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for CharField. Passes fieldName to the constructor of the
	 * Field class. Extracts and stores the length of the string.
	 * 
	 * @param fieldName
	 *            The name of the CharField being created.
	 * @param type
	 *            String containing an integer defining the length of the string
	 *            stored by CharField.
	 */
	public CharField(String fieldName, int position, int charLength) {
		super(fieldName, position);
		byteSize = charLength * 2;
	}

	/**
	 * Gets the length.
	 * 
	 * @return the length
	 */
	public int getLength() {
		return byteSize / 2;
	}

	public CharValue makeValue(String value) throws OmniException {
		if (this.getLength() != value.length())
			throw new OmniException(value
					+ " does not fit into type Char with length "
					+ this.getLength() + "!");
		return new CharValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jdavis31.data.fields.Field#read(java.io.RandomAccessFile)
	 */
	@Override
	public CharValue read(RandomAccessFile binFile) throws IOException {
		return new CharValue(binFile, getLength());
	}

	@Override
	public CharField rename(String newName) {
		return new CharField(newName, this.position, this.byteSize);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jdavis31.data.fields.Field#toString()
	 */
	@Override
	public String toString() {
		return getName() + " (" + getType() + "[" + getLength() + "]" + ")";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jdavis31.data.fields.Field#toXMLString()
	 */
	@Override
	public String toXMLString() {
		return "\t\t<FIELD NAME=\"" + getName() + "\" TYPE=\"" + getType()
				+ "\" LENGTH=\"" + getLength() + "\" INDEX=\""
				+ hasIndex() + "\" />\n";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jdavis31.data.fields.Field#write(java.io.RandomAccessFile,
	 * java.lang.String)
	 */
	@Override
	public void write(RandomAccessFile binFile, String value)
			throws IOException, OmniException {
		if (this.getLength() != Utilities.removeQuotes(value).length())
			throw new OmniException(value
					+ " does not fit into type Char with length "
					+ this.getLength() + "!");
		CharValue newValue = new CharValue(value);
		newValue.write(binFile);
	}
}