package jdavis31.data.fields;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Serializable;

import jdavis31.Driver;
import jdavis31.OmniException;
import jdavis31.data.BinarySearchTree;
import jdavis31.data.values.Value;

/**
 * A Field provides information about, as well as access to, a single field of a
 * Table.
 * 
 * @author Joshua Davis
 */
public abstract class Field implements Serializable {

	private static final long serialVersionUID = 1L;
	protected String fieldName;
	protected int byteSize;
	protected int position;
	private BinarySearchTree index;

	/**
	 * Constructor for the Field class. Sets the fieldName stored by Field
	 * objects to the input fieldName.
	 * 
	 * @param fieldName
	 *            Name for the field being created.
	 * @param position
	 *            the position
	 */
	public Field(String fieldName, int position) {
		this.fieldName = fieldName;
		this.position = position;
	}

	public void defineIndex(BinarySearchTree index) {
		this.index = index;
	}
	
	/**
	 * Returns the number of bytes required to store a field's data.
	 * 
	 * @return The number of bytes required.
	 */
	public int getByteSize() {
		return byteSize;
	}
	
	public BinarySearchTree getIndex() {
		return index;
	}

	/**
	 * Returns the name of a Field;.
	 * 
	 * @return The stored value of fieldName.
	 */
	public String getName() {
		return fieldName;
	}

	/**
	 * Returns the position of the field in a row.
	 * 
	 * @return The position, in bytes.
	 */
	public int getPos() {
		return position;
	}

	/**
	 * Returns the type of a field as a string.
	 * 
	 * @return The string representation of a Field's type.
	 */
	public String getType() {
		return this.getClass().getSimpleName().replace("Field", "")
				.toLowerCase();
	}
	
	public boolean hasIndex() {
		return index != null;
	}

	@SuppressWarnings("rawtypes")
	public void insertIndex(Value newValue, long rowPos,
			RandomAccessFile binFile) throws OmniException, IOException {
		if (index != null) {
			if (Driver.debug)
				System.out.println("Inserting " + rowPos + " into index "
						+ fieldName + ".");
			index.insert(newValue, rowPos, index.getRoot(), binFile);
		}
	}

	@SuppressWarnings("rawtypes")
	public abstract Value makeValue(String value) throws OmniException;

	/**
	 * Reads a value from a field in a table.
	 * 
	 * @param binFile
	 *            The table's storage file.
	 * @return A value object containing a Field's data.
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings("rawtypes")
	public abstract Value read(RandomAccessFile binFile) throws IOException;

	public abstract Field rename(String newName);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return getName() + " (" + getType() + ")";
	}

	/**
	 * Generates an XML representation of the field.
	 * 
	 * @return XML representation of the field.
	 */
	public String toXMLString() {
		return "\t\t<FIELD NAME=\"" + getName() + "\" TYPE=\"" + getType()
				+ "\" INDEX=\"" + hasIndex() + "\" />\n";
	}

	@SuppressWarnings("rawtypes")
	public void updateIndex(Value oldValue, Value newValue, long rowPos,
			RandomAccessFile tableData) throws OmniException {
		index.delete(oldValue, index.getRoot(), tableData);
		index.insert(newValue, rowPos, index.getRoot(), tableData);
	}

	public abstract void write(RandomAccessFile binFile, String value)
			throws IOException, OmniException;
}
