package jdavis31.data.fields;

import java.io.IOException;
import java.io.RandomAccessFile;

import jdavis31.FileStructure;
import jdavis31.OmniException;
import jdavis31.data.values.VarCharValue;

/**
 * A VarCharField provides information about, as well as access to, a single
 * variable character field of a Table.
 * 
 * @author Joshua Davis
 */
public class VarCharField extends Field implements FileStructure {
	
	private static final long serialVersionUID = 1L;
	final int size = 8;

	/**
	 * The constructor for the VarCharField class. Simply passes fieldName to
	 * the constructor of the Field class.
	 *
	 * @param fieldName
	 *            The name of the VarCharField being created.
	 */
	public VarCharField(String fieldName, int position) {
		super(fieldName, position);
		byteSize = 8;
	}
	
	public VarCharValue makeValue(String value) throws OmniException {
		return new VarCharValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jdavis31.data.fields.Field#read(java.io.RandomAccessFile)
	 */
	@Override
	public VarCharValue read(RandomAccessFile binFile) throws IOException {
		return new VarCharValue(binFile);
	}
	
	@Override
	public VarCharField rename(String newName) {
		return new VarCharField(newName, this.position);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see jdavis31.data.fields.Field#write(java.io.RandomAccessFile,
	 * java.lang.String)
	 */
	@Override
	public void write(RandomAccessFile binFile, String value)
			throws OmniException, IOException {
		VarCharValue newValue = new VarCharValue(value);
		newValue.write(binFile);
	}
}