package jdavis31.data.fields;

import java.io.IOException;
import java.io.RandomAccessFile;

import jdavis31.OmniException;
import jdavis31.data.values.IntegerValue;

/**
 * An IntegerField provides information about, as well as access to, a single integer field of a Table.
 * 
 * @author Joshua Davis
 */
public class IntegerField extends Field {
	
	private static final long serialVersionUID = 1L;

	/**
	 * The constructor for IntegerField. Simply passes fieldName to the
	 * constructor of the Field class.
	 *
	 * @param fieldName
	 *            The name for the IntegerField being created.
	 */
	public IntegerField(String fieldName, int position) {
		super(fieldName, position);
		byteSize = 4;
	}
	
	public IntegerValue makeValue(String value) throws OmniException {
		return new IntegerValue(value);
	}

	/* (non-Javadoc)
	 * @see jdavis31.data.fields.Field#read(java.io.RandomAccessFile)
	 */
	@Override
	public IntegerValue read(RandomAccessFile binFile) throws IOException {
		return new IntegerValue(binFile);
	}
	
	@Override
	public IntegerField rename(String newName) {
		return new IntegerField(newName, this.position);
	}
	
	/* (non-Javadoc)
	 * @see jdavis31.data.fields.Field#write(java.io.RandomAccessFile, java.lang.String)
	 */
	@Override
	public void write(RandomAccessFile binFile, String value)
			throws OmniException, IOException {
		IntegerValue newValue = new IntegerValue(value);
		newValue.write(binFile);
	}
}
