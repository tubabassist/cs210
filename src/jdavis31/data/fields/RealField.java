package jdavis31.data.fields;

import java.io.IOException;
import java.io.RandomAccessFile;

import jdavis31.OmniException;
import jdavis31.data.values.RealValue;

/**
 * A RealField provides information about, as well as access to, a single real
 * number field of a Table.
 * 
 * @author Joshua Davis
 */
public class RealField extends Field {
	
	private static final long serialVersionUID = 1L;

	/**
	 * The constructor for the RealField class. Simply passes the fieldName to
	 * the constructor of the Field class.
	 *
	 * @param fieldName
	 *            The name for the RealField being created.
	 */
	public RealField(String fieldName, int position) {
		super(fieldName, position);
		byteSize = 8;
	}
	
	public RealValue makeValue(String value) throws OmniException {
		return new RealValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jdavis31.data.fields.Field#read(java.io.RandomAccessFile)
	 */
	@Override
	public RealValue read(RandomAccessFile binFile) throws IOException {
		return new RealValue(binFile);
	}
	
	@Override
	public RealField rename(String newName) {
		return new RealField(newName, this.position);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see jdavis31.data.fields.Field#write(java.io.RandomAccessFile,
	 * java.lang.String)
	 */
	@Override
	public void write(RandomAccessFile binFile, String value)
			throws OmniException, IOException {
		RealValue newValue = new RealValue(value);
		newValue.write(binFile);
	}
}
