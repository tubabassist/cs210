package jdavis31.data.fields;

import java.io.IOException;
import java.io.RandomAccessFile;

import jdavis31.OmniException;
import jdavis31.data.values.DateValue;

/**
 * A DateField provides information about, as well as access to, a single date
 * field of a Table.
 * 
 * @author Joshua Davis
 */
public class DateField extends Field {

	private static final long serialVersionUID = 1L;

	/**
	 * The constructor for DateField. Simply passes fieldName to the constructor
	 * of the Field class.
	 * 
	 * @param fieldName
	 *            The name of the DateField being created.
	 */
	public DateField(String fieldName, int position) {
		super(fieldName, position);
		byteSize = 8;
	}

	public DateValue makeValue(String value) throws OmniException {
		return new DateValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jdavis31.data.fields.Field#read(java.io.RandomAccessFile)
	 */
	@Override
	public DateValue read(RandomAccessFile binFile) throws IOException {
		return new DateValue(binFile);
	}

	@Override
	public DateField rename(String newName) {
		return new DateField(newName, this.position);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jdavis31.data.fields.Field#write(java.io.RandomAccessFile,
	 * java.lang.String)
	 */
	@Override
	public void write(RandomAccessFile binFile, String value)
			throws IOException, OmniException {
		DateValue newValue = new DateValue(value);
		newValue.write(binFile);
	}
}
