package jdavis31.data;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

import jdavis31.data.values.CharValue;
import jdavis31.data.values.Value;

@SuppressWarnings("rawtypes")
public class Row implements Comparable<Row>, Serializable {
	
	private static final long serialVersionUID = 1L;
	protected ArrayList<Value> values;
	
	public Row() {
		values = new ArrayList<Value>();
	}

	public void add(Value value) {
		values.add(value);
	}

	@SuppressWarnings("unchecked")
	@Override
	public int compareTo(Row newRow) {
		for (int i = 0; i < this.values.size(); i++) {
			int compareTo = this.values.get(i).compareTo(newRow.values.get(i));
			if (compareTo != 0)
				return compareTo;
		}
		return 0;
	}
	
	public boolean contains(Value value) {
		return values.contains(value);
	}

	public Row join(Row newRow) {
		Row join = new Row();
		for (Value value : this.values)
			join.add(value);
		for (Value value : newRow.values)
			join.add(value);
		return join;
	}

	public boolean matches(Row newRow) {
		if (this.values.size() != newRow.values.size())
			return false;
		for (int i = 0; i < this.values.size(); i++) {
			if (!this.values.get(i).getClass()
					.equals(newRow.values.get(i).getClass()))
				return false;
			if (this.values.get(i).getClass().equals(CharValue.class)) {
				if (this.values.get(i).toString().length() != newRow.values
						.get(i).toString().length())
					return false;
			}
		}
		return true;
	}

	public static Comparator<Row> orderBy(final int fieldPos) {
		return new Comparator<Row>() {

			@SuppressWarnings("unchecked")
			@Override
			public int compare(Row row1, Row row2) {
				return row1.values.get(fieldPos).compareTo(
						row2.values.get(fieldPos));
			}
		};
	}

	public String toString() {
		String rows = "";
		for (Value value : values) {
			rows += value.toString() + "\t";
		}
		return rows;
	}
	
	public Value valueAt(int i) {
		return values.get(i);
	}
	
	public void write(RandomAccessFile binFile) throws IOException {
		binFile.seek(binFile.length());
		binFile.writeBoolean(true);
		for (Value value : values) {
			value.write(binFile);
		}
	}
}
