package jdavis31.data.values;

import java.io.IOException;
import java.io.RandomAccessFile;

import jdavis31.FileStructure;
import jdavis31.OmniException;
import jdavis31.utilities.Utilities;

/**
 * A Value provides a common container object for passing around variable
 * character values stored by a table.
 */
public class VarCharValue extends Value<VarCharValue> implements FileStructure {
	
	private static final long serialVersionUID = 1L;
	protected final String VALUE;
	
	public VarCharValue(RandomAccessFile binFile) throws IOException {
		RandomAccessFile varchars = new RandomAccessFile(VARCHAR_FILE, "r");
		long reference = binFile.readLong();
		varchars.seek(reference);
		this.VALUE = varchars.readUTF();
		varchars.close();
	}
	
	/**
	 * Constructor for a variable character value.
	 * 
	 * @param value
	 *            String object to be stored.
	 * @throws OmniException 
	 */
	public VarCharValue(String value) throws OmniException {
		this.VALUE = Utilities.removeQuotes(value);
	}
	
	@Override
	public int compareTo(VarCharValue newValue) {
		return this.VALUE.compareTo(newValue.VALUE);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see jdavis31.data.values.Value#toString()
	 */
	@Override
	public String toString() {
		return VALUE;
	}

	@Override
	public void write(RandomAccessFile binFile) throws IOException {
		RandomAccessFile varchars = new RandomAccessFile(VARCHAR_FILE, "rw");
		long reference = varchars.length();
		varchars.seek(reference);
		varchars.writeUTF(VALUE);
		varchars.close();
		binFile.writeLong(reference);
	}
}
