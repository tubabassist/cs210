package jdavis31.data.values;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import jdavis31.OmniException;
import jdavis31.utilities.Utilities;

/**
 * A Value provides a common container object for passing around date values
 * stored by a table.
 */
public class DateValue extends Value<DateValue> {
	
	private static final long serialVersionUID = 1L;
	protected final Date VALUE;
	protected final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
			"MM/dd/yyyy");

	public DateValue(RandomAccessFile binFile) throws IOException {
		this.VALUE = new Date(binFile.readLong());
	}

	/**
	 * Constructor for a date value.
	 * 
	 * @param value
	 *            Date object to be stored.
	 * @throws OmniException
	 */
	public DateValue(String value) throws OmniException {
		try {
			this.VALUE = DATE_FORMAT.parse(Utilities.removeQuotes(value).trim());
		} catch (ParseException e) {
			throw new OmniException(
					value
							+ " cannot be placed in type Date with format 'MM/dd/yyyy'!");
		}
	}

	@Override
	public int compareTo(DateValue newValue) {
		return this.VALUE.compareTo(newValue.VALUE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jdavis31.data.values.Value#toString()
	 */
	@Override
	public String toString() {
		return DATE_FORMAT.format(VALUE);
	}

	@Override
	public void write(RandomAccessFile binFile) throws IOException {
		binFile.writeLong(VALUE.getTime());
	}
}
