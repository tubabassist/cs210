package jdavis31.data.values;

import java.io.IOException;
import java.io.RandomAccessFile;

import jdavis31.OmniException;

/**
 * A Value provides a common container object for passing around boolean values
 * stored by a table.
 */
public class BooleanValue extends Value<BooleanValue> {
	
	private static final long serialVersionUID = 1L;
	protected final boolean VALUE;

	public BooleanValue(RandomAccessFile binFile) throws IOException {
		this.VALUE = binFile.readBoolean();
	}

	/**
	 * Constructor for a boolean value.
	 * 
	 * @param value
	 *            Boolean object to be stored.
	 * @throws OmniException
	 */
	public BooleanValue(String value) throws OmniException {
		if (!value.matches("true|false"))
			throw new OmniException("'" + value
					+ "' does not fit into type Boolean!");
		this.VALUE = Boolean.parseBoolean(value);
	}

	@Override
	public int compareTo(BooleanValue newValue) {
		return (this.VALUE == newValue.VALUE ? 0 : (this.VALUE ? 1 : -1));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jdavis31.data.values.Value#toString()
	 */
	@Override
	public String toString() {
		return String.valueOf(VALUE);
	}

	@Override
	public void write(RandomAccessFile binFile) throws IOException {
		binFile.writeBoolean(VALUE);
	}
}