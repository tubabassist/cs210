package jdavis31.data.values;

import java.io.IOException;
import java.io.RandomAccessFile;
import jdavis31.OmniException;

/**
 * A Value provides a common container object for passing around integer values
 * stored by a table.
 */
public class IntegerValue extends Value<IntegerValue> {
	
	private static final long serialVersionUID = 1L;
	protected final int VALUE;

	public IntegerValue(RandomAccessFile binFile) throws IOException {
		this.VALUE = binFile.readInt();
	}

	/**
	 * Constructor for an integer value.
	 * 
	 * @param value
	 *            Integer to be stored.
	 * @throws OmniException
	 */
	public IntegerValue(String value) throws OmniException {
		try {
			this.VALUE = Integer.parseInt(value);
		} catch (NumberFormatException e) {
			throw new OmniException("'" + value
					+ "' does not fit into type Integer!");
		}
	}

	@Override
	public int compareTo(IntegerValue newValue) {
		return (this.VALUE < newValue.VALUE ? -1
				: (this.VALUE == newValue.VALUE ? 0 : 1));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jdavis31.data.values.Value#toString()
	 */
	@Override
	public String toString() {
		return String.valueOf(VALUE);
	}

	@Override
	public void write(RandomAccessFile binFile) throws IOException {
		binFile.writeInt(VALUE);
	}
}
