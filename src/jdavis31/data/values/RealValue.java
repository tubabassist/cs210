package jdavis31.data.values;

import java.io.IOException;
import java.io.RandomAccessFile;

import jdavis31.OmniException;

/**
 * A Value provides a common container object for passing around real number
 * values stored by a table.
 */
public class RealValue extends Value<RealValue> {
	
	private static final long serialVersionUID = 1L;
	protected final double VALUE;
	
	public RealValue(RandomAccessFile binFile) throws IOException {
		this.VALUE = binFile.readDouble();
	}

	/**
	 * Constructor for a real value.
	 * 
	 * @param value
	 *            Double to be stored.
	 * @throws OmniException 
	 */
	public RealValue(String value) throws OmniException {
		try{
			this.VALUE = Double.parseDouble(value);
		} catch (NumberFormatException e) {
			throw new OmniException("'" + value
					+ "' does not fit into type Real!");
		}
	}

	@Override
	public int compareTo(RealValue newValue) {
		return (this.VALUE < newValue.VALUE ? -1
				: (this.VALUE == newValue.VALUE ? 0 : 1));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jdavis31.data.values.Value#toString()
	 */
	@Override
	public String toString() {
		return String.valueOf(VALUE);
	}

	@Override
	public void write(RandomAccessFile binFile) throws IOException {
		binFile.writeDouble(VALUE);
	}
}
