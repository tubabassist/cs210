package jdavis31.data.values;

import java.io.IOException;
import java.io.RandomAccessFile;

import jdavis31.OmniException;
import jdavis31.utilities.Utilities;

/**
 * A Value provides a common container object for passing around fixed-width
 * character values stored by a table.
 */
public class CharValue extends Value<CharValue> {
	
	private static final long serialVersionUID = 1L;
	protected final String VALUE;

	public CharValue(RandomAccessFile binFile, int length) throws IOException {
		String chars = "";
		for (int i = 0; i < length; i++) {
			chars += binFile.readChar();
		}
		this.VALUE = chars;
	}

	/**
	 * Constructor for a character value.
	 * 
	 * @param value
	 *            String object to be stored.
	 * @throws OmniException
	 */
	public CharValue(String value) throws OmniException {
		this.VALUE = Utilities.removeQuotes(value);
	}

	@Override
	public int compareTo(CharValue newValue) {
		return this.VALUE.compareTo(newValue.VALUE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jdavis31.data.values.Value#toString()
	 */
	@Override
	public String toString() {
		return VALUE;
	}

	@Override
	public void write(RandomAccessFile binFile) throws IOException {
		binFile.writeChars(VALUE);
	}
}
