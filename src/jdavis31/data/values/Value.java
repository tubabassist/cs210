package jdavis31.data.values;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Serializable;

/**
 * A Value provides a common container object for passing around the various
 * data types stored in a table.
 * @param <V>
 */
@SuppressWarnings("rawtypes")
public abstract class Value<T extends Value> implements Comparable<T>, Serializable {
	
	private static final long serialVersionUID = 1L;
	public abstract int compareTo(T newValue);
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public abstract String toString();
	
	public abstract void write(RandomAccessFile binFile) throws IOException;
}