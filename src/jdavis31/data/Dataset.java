package jdavis31.data;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import jdavis31.OmniException;
import jdavis31.data.fields.Field;

public class Dataset implements Serializable {

	private static final long serialVersionUID = 1L;
	private ArrayList<Row> rows;
	private ArrayList<Field> fields;
	private String dataName;

	public Dataset(Table table) throws OmniException {
		this.rows = new ArrayList<Row>();
		this.dataName = table.getName();
		this.fields = new ArrayList<Field>(table.fields);
	}

	public Dataset(Dataset newDataset) {
		this.rows = new ArrayList<Row>(newDataset.rows);
		this.fields = new ArrayList<Field>(newDataset.fields);
		this.dataName = newDataset.dataName;
	}

	public Dataset(String tableName, ArrayList<Field> fields) {
		this.rows = new ArrayList<Row>();
		this.fields = new ArrayList<Field>(fields);
		this.dataName = tableName;
	}

	public void add(Row row) {
		rows.add(row);
	}

	public void addAll(Dataset newDataset) {
		rows.addAll(newDataset.rows);
	}

	public boolean contains(Row row1) {
		for (Row row2 : this.rows)
			if (row2.compareTo(row1) == 0)
				return true;
		return false;
	}

	public Dataset intersect(Dataset newDataset) throws OmniException {
		if (this.matches(newDataset)) {
			Dataset intersect = new Dataset(this.dataName, this.fields);
			for (Row row : this.rows) {
				if (newDataset.contains(row)) {
					intersect.add(row);
				}
			}
			return intersect;
		} else
			throw new OmniException("These data sets are not union compatible.");
	}

	public boolean isEmpty() {
		return this.rows.isEmpty();
	}

	public Dataset join(Dataset newDataset) {
		ArrayList<Field> combinedFields = new ArrayList<Field>();
		for (Field field : this.fields) {
			combinedFields.add(field.rename(this.dataName + "."
					+ field.getName()));
		}
		for (Field field : newDataset.fields) {
			combinedFields.add(field.rename(this.dataName + "."
					+ field.getName()));
		}

		Dataset join = new Dataset(this.dataName, combinedFields);
		for (Row row1 : this.rows) {
			for (Row row2 : newDataset.rows) {
				join.add(row1.join(row2));
			}
		}
		return join;
	}

	public boolean matches(Dataset newDataset) {
		return this.rows.get(0).matches(newDataset.rows.get(0));
	}

	public Dataset minus(Dataset newDataset) throws OmniException {
		if (this.matches(newDataset)) {
			Dataset minus = new Dataset(this.dataName, this.fields);
			for (Row row : this.rows) {
				if (!newDataset.contains(row)) {
					minus.add(row);
				}
			}
			return minus;
		} else
			throw new OmniException("These data sets are not union compatible.");
	}

	public Dataset orderBy(String fieldName, boolean decending)
			throws OmniException {
		Dataset ordered = new Dataset(this);
		int fieldPos = -1;

		for (int i = 0; i < this.fields.size(); i++) {
			if (fieldName.toLowerCase().equals(fields.get(i).getName())) {
				fieldPos = i;
				break;
			}
		}
		if (fieldPos == -1)
			throw new OmniException("Field '" + fieldName.toLowerCase()
					+ "' is not defined for this data set.");

		if (decending)
			Collections.sort(ordered.rows,
					Collections.reverseOrder(Row.orderBy(fieldPos)));
		else
			Collections.sort(ordered.rows, Row.orderBy(fieldPos));

		return ordered;
	}

	public Dataset project(String fieldList) throws OmniException {
		String[] projectedNames = fieldList.split(",");
		ArrayList<Integer> fieldPos = new ArrayList<Integer>();
		ArrayList<Field> projectedFields = new ArrayList<Field>();

		for (int i = 0; i < projectedNames.length; i++) {
			projectedNames[i] = projectedNames[i].trim().toLowerCase();
			for (int j = 0; j < fields.size(); j++) {
				if (projectedNames[i].equals(fields.get(j).getName())) {
					fieldPos.add(j);
					projectedFields.add(fields.get(j));
				}
			}
			if (fieldPos.size() != i + 1)
				throw new OmniException("Field '" + projectedNames[i]
						+ "' is not defined for this data set.");
		}

		Dataset projectedData = new Dataset(this.dataName, projectedFields);
		for (Row row : this.rows) {
			Row projectedRow = new Row();
			for (Integer pos : fieldPos) {
				projectedRow.add(row.valueAt(pos));
			}
			projectedData.add(projectedRow);
		}
		return projectedData;
	}

	public Dataset select(String whereClause) throws OmniException {
		WhereStatement parsedWhere = new WhereStatement(whereClause, fields);
		Dataset newDataset = new Dataset(dataName, fields);

		Iterator<Row> it = parsedWhere.iterator(rows);
		while (it.hasNext())
			newDataset.add(it.next());

		/*
		 * for (Row row : rows) { if (parsedWhere.evaluate(row)) {
		 * newDataset.add(row); } }
		 */
		return newDataset;
	}

	public String toString() {
		String dataset = "\n" + this.dataName + ":\n";
		for (Field field : this.fields) {
			dataset += "[" + field.getName() + "]\t";
		}
		dataset += "\n";
		for (Row row : rows) {
			dataset += row.toString() + "\n";
		}
		dataset += "\n<-- End of data set -->\n";
		return dataset;
	}

	public Dataset union(Dataset newDataset) throws OmniException {
		if (this.matches(newDataset)) {
			Dataset union = new Dataset(this);
			for (Row row : newDataset.rows)
				if (!this.contains(row))
					union.add(row);
			return union;
		} else
			throw new OmniException("These data sets are not union compatible.");
	}

	public void write(RandomAccessFile binFile) throws IOException {
		for (Row row : rows)
			row.write(binFile);
	}
}