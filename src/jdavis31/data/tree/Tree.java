package jdavis31.data.tree;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

import jdavis31.FileStructure;
import jdavis31.OmniException;
import jdavis31.data.Row;

public class Tree implements FileStructure {

	public Tree(String tableName, RandomAccessFile tableData, String fieldName) throws OmniException {
		try {
			new RandomAccessFile(DIRECTORY + File.separator
					+ tableName + File.separator + fieldName, "rw");
		} catch (IOException e) {
			throw new OmniException(
					"Error defining index for table '"
							+ tableName
							+ "'!  You may not have appropriate read/write permissions in this location.");
		}
	}

	public void delete(Node node, Node root) {
		
	}

	public ArrayList<Long> inOrderWalk(Node root) {
		ArrayList<Long> positions = new ArrayList<Long>();
		
		
		
		return positions;
	}
	
	public void insert(Row row, Node root) {
		
	}

	public Node max(Node root) {
		try {
			return root.getRightChild();
		} catch (IOException e) {
			return root;
		}
	}

	public Node min(Node root) {
		try {
			return root.getLeftChild();
		} catch (IOException e) {
			return root;
		}
	}

	public Node predecessor(Node root) {
		try {
			return max(root.getLeftChild());
		} catch (IOException e) {
			return root;
		}
	}

	public ArrayList<Long> reverseInOrderWalk(Node root) {
		ArrayList<Long> positions = new ArrayList<Long>();
		
		
		
		return positions;
	}
	
	public ArrayList<Long> select(Row row, Node root) {
		ArrayList<Long> positions = new ArrayList<Long>();
		
		
		
		return positions;
	}

	public Node successor(Node root) {
		try {
			return min(root.getRightChild());
		} catch (IOException e) {
			return root;
		}
	}
}
