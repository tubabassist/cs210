package jdavis31.data.tree;

import java.io.IOException;
import java.io.RandomAccessFile;

public class Node {
	
	/*
	 * One node = 32 bytes
	 * 1) Value, bytes 0 - 7
	 * 2) Parent, bytes 8 - 15
	 * 3) Left child, bytes 16 - 23
	 * 4) Right child, bytes 24 - 31
	 */
	
	private RandomAccessFile tableData;
	private long location;
	private static final short PARENT_RELATIVE_LOCATION = 8;
	private static final short LEFT_CHILD_RELATIVE_LOCATION = 16;
	private static final short RIGHT_CHILD_RELATIVE_LOCATION = 24;
	
	public Node(RandomAccessFile binFile, Long location) throws IOException {
		this.tableData = binFile;
		this.location = location;
	}
	
	public Node getLeftChild() throws IOException {
		return new Node(this.tableData, this.location + LEFT_CHILD_RELATIVE_LOCATION);
	}
	
	public Node getRightChild() throws IOException {
		return new Node(this.tableData, this.location + RIGHT_CHILD_RELATIVE_LOCATION);
	}
	
	public Node getParent() throws IOException {
		return new Node(this.tableData, this.location + PARENT_RELATIVE_LOCATION);
	}
	
	public Node getRow() throws IOException {
		return new Node(this.tableData, this.location);
	}
	
	public void setLeftChild(Long location) throws IOException {
		tableData.seek(this.location + LEFT_CHILD_RELATIVE_LOCATION);
		tableData.writeLong(location);
	}
	
	public void setRightChild() throws IOException {
		tableData.seek(this.location + RIGHT_CHILD_RELATIVE_LOCATION);
		tableData.writeLong(location);
	}
	
	public void setParent() throws IOException {
		tableData.seek(this.location + PARENT_RELATIVE_LOCATION);
		tableData.writeLong(location);
	}
	
	public void setRow() throws IOException {
		tableData.seek(this.location);
		tableData.writeLong(location);
	}
}
