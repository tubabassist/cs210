package jdavis31.data;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Iterator;

import jdavis31.Driver;
import jdavis31.FileStructure;
import jdavis31.OmniException;
import jdavis31.data.fields.BooleanField;
import jdavis31.data.fields.CharField;
import jdavis31.data.fields.DateField;
import jdavis31.data.fields.Field;
import jdavis31.data.fields.IntegerField;
import jdavis31.data.fields.RealField;
import jdavis31.data.fields.VarCharField;
import jdavis31.data.values.Value;

/**
 * A Table provides information about, as well as access to, a single table.
 * Information stored includes the table name, and all of the fields in the
 * table.
 * 
 * @author Joshua Davis
 */
public class Table implements FileStructure, Serializable {

	private static final long serialVersionUID = 1L;
	private final String DATA_NAME = "DATA";
	private String tableName;
	protected ArrayList<Field> fields;
	transient protected RandomAccessFile binFile;
	private int rowSize;
	private Dataset data;

	/**
	 * Instantiates a new table.
	 * 
	 * @param tableName
	 *            Name of the table to be created.
	 * @throws OmniException
	 *             The constructor for the Table class. Assigns tableName and
	 *             creates a list of Fields.
	 */
	public Table(String tableName) throws OmniException {
		try {
			this.tableName = tableName.toUpperCase();
			this.fields = new ArrayList<Field>();
			this.rowSize = 1;
			File fileStructure = new File(DIRECTORY + File.separator
					+ this.tableName);
			fileStructure.mkdirs();
			binFile = new RandomAccessFile(fileStructure.getAbsolutePath()
					+ File.separator + DATA_NAME, "rw");
		} catch (IOException e) {
			throw new OmniException(
					"Error creating table '"
							+ this.tableName
							+ "'!  You may not have appropriate read/write permissions in this location.");
		}
	}

	/**
	 * The constructor for the Table class. Assigns tableName and creates a list
	 * of Fields. Parses an extendedFieldList, and creates each field in the
	 * table.
	 * 
	 * @param tableName
	 *            Name of the table to be created.
	 * @param extendedFieldList
	 *            Field names and types, each field name and type grouped
	 *            together between commas.
	 * @throws OmniException
	 *             Thrown if a field type is invalid or if a field name already
	 *             exists in the table.
	 */
	public Table(String tableName, String extendedFieldList)
			throws OmniException {
		this(tableName);

		String[] extendedFields = extendedFieldList.split(",");

		for (String extendedField : extendedFields) {
			String[] input = extendedField.trim().split("\\s+");
			if (input[1].equalsIgnoreCase("char") && input.length > 2) {
				String type = "";
				for (int i = 1; i < input.length; i++) {
					type += input[i];
				}
				add(input[0], type);
			} else {
				add(input[0], input[1]);
			}
		}
	}

	/**
	 * Creates and adds a field to the table.
	 * 
	 * @param fieldName
	 *            Name of the field to be created.
	 * @param type
	 *            Type of the field to be created.
	 * @throws OmniException
	 *             Thrown if the new field name already exists in the table.
	 * @throws IOException
	 */
	protected void add(String fieldName, String type) throws OmniException {

		fieldName = fieldName.toLowerCase();
		type = type.toLowerCase();
		Field newField = null;
		for (Field existingField : fields) {
			if (fieldName.matches(existingField.getName())) {
				throw new OmniException(
						"Cannot define multiple fields with name '" + fieldName
								+ "'!");
			}
		}
		switch (type) {
		case "integer":
			newField = new IntegerField(fieldName, rowSize);
			break;
		case "date":
			newField = new DateField(fieldName, rowSize);
			break;
		case "real":
			newField = new RealField(fieldName, rowSize);
			break;
		case "varchar":
			newField = new VarCharField(fieldName, rowSize);
			break;
		case "boolean":
			newField = new BooleanField(fieldName, rowSize);
			break;
		default:
			if (type.matches("char\\(\\d+\\)")) {
				newField = new CharField(fieldName, rowSize,
						Integer.parseInt(type.replaceAll("[\\D]", "")));
			} else {
				throw new OmniException("'" + type
						+ "' is not a valid field type!");
			}
		}
		fields.add(newField);
		rowSize += newField.getByteSize();
	}

	public void defineExistingIndex(String fieldName) throws OmniException {
		for (Field field : fields) {
			if (field.getName().equalsIgnoreCase(fieldName.trim())) {
				BinarySearchTree index = new BinarySearchTree(tableName, field,
						false);
				field.defineIndex(index);
				return;
			}
		}
	}

	public void defineIndex(String fieldName) throws OmniException {
		try {
			for (Field field : fields) {
				if (field.getName().equalsIgnoreCase(fieldName.trim())) {
					BinarySearchTree index = new BinarySearchTree(tableName,
							field, true);
					long rowPos = 0;
					while (rowPos < binFile.length()) {
						binFile.seek(rowPos);
						if (binFile.readBoolean()) {
							binFile.seek(rowPos + field.getPos());
							index.insert(field.read(binFile), rowPos,
									index.getRoot(), binFile);
						}
						rowPos += rowSize;
					}
					field.defineIndex(index);
					return;
				}
			}
			throw new OmniException("Field '" + fieldName.trim().toLowerCase()
					+ "' is not defined.");

		} catch (IOException e) {
			throw new OmniException(
					"\nAn error has occured while reading table '" + tableName
							+ "'.  This table may be corrupt!\n");
		}
	}

	/**
	 * Removes rows of data from the table.
	 * 
	 * @param whereClause
	 *            Rows are deleted when this where clause evaluates true.
	 * @throws OmniException
	 *             Thrown if a field is not defined, or if there is an error
	 *             reading or writing to the table files.
	 */
	@SuppressWarnings("rawtypes")
	public void delete(String whereClause) throws OmniException {
		WhereStatement parsedWhere = new WhereStatement(whereClause, fields);

		try {
			Iterator<Long> it = parsedWhere.iterator(binFile, rowSize);
			while (it.hasNext()) {
				long pos = it.next();
				binFile.seek(pos);
				binFile.writeBoolean(false);
				for (Field field : fields) {
					if (field.getIndex() != null) {
						binFile.seek(pos + field.getPos());
						Value delValue = field.read(binFile);
						if (Driver.debug)
							System.out.println("Deleting "
									+ delValue.toString() + " from index "
									+ field.getName() + ".");
						field.getIndex().delete(delValue,
								field.getIndex().getRoot(), binFile);
					}
				}
			}
		} catch (IOException e) {
			if (Driver.debug) {
				e.printStackTrace();
				System.out.println("DEBUG = " + Driver.debug
						+ ", stack trace printed.");
			}
			throw new OmniException(
					"\nAn error has occured while reading table '" + tableName
							+ "'.  This table may be corrupt.\n");
		}
	}

	/**
	 * Deletes the file that stores a table's data. Used to delete a table's
	 * data when a table is deleted from the database.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void deleteBin() throws IOException {
		binFile.close();
		for (Field field : fields) {
			if (field.hasIndex())
				field.getIndex().deleteBin(tableName);
		}
		Files.deleteIfExists(new File(DIRECTORY + File.separator
				+ this.tableName + File.separator + DATA_NAME).toPath());
		Files.deleteIfExists(new File(DIRECTORY, tableName).toPath());
	}

	/**
	 * Selects rows of data from a table.
	 * 
	 * @param parsedWhere
	 *            Rows are selected when this where clause evaluates true.
	 * @return A string representation of the selected rows.
	 * @throws OmniException
	 *             Thrown if a table is not defined, a field is not defined, or
	 *             if there is an error reading or writing to the table files.
	 */
	public Dataset getData() throws OmniException {
		this.data = new Dataset(this);
		WhereStatement parsedWhere = new WhereStatement(null, fields);

		try {
			Iterator<Long> it = parsedWhere.iterator(binFile, rowSize);
			while (it.hasNext()) {
				binFile.seek(it.next() + 1);
				Row row = new Row();
				for (Field field : this.fields) {
					row.add(field.read(binFile));
				}
				this.data.add(row);
			}
		} catch (IOException e) {
			if (Driver.debug) {
				e.printStackTrace();
				System.out.println("DEBUG = " + Driver.debug
						+ ", stack trace printed.");
			}
			throw new OmniException(
					"\nAn error has occured while reading table '" + tableName
							+ "'.  This table may be corrupt!\n");
		}
		if (this.data.isEmpty())
			throw new OmniException("Table '" + tableName
					+ "' contains no values!");
		return this.data;
	}

	/**
	 * Provides the name of a Table.
	 * 
	 * @return The name of a Table.
	 */
	public String getName() {
		return this.tableName;
	}

	/**
	 * Writes a row of data at the end of a table.
	 * 
	 * @param valueList
	 *            Row of data to be inserted into the table.
	 * @throws OmniException
	 *             Thrown if there is a problem with the provided valueList, or
	 *             if the file that stores the table data is corrupt.
	 */
	public void insert(String valueList) throws OmniException {
		long lengthBefore = 0;
		try {
			lengthBefore = binFile.length();
			String[] values = valueList
					.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
			if (values.length != fields.size()) {
				throw new OmniException("'" + this.tableName + "' has "
						+ fields.size() + " fields.");
			}
			Long rowPos = binFile.length();
			binFile.seek(rowPos);
			binFile.writeBoolean(true);
			for (int i = 0; i < fields.size(); i++) {
				fields.get(i).write(binFile, values[i].trim());
			}
		} catch (OmniException | IOException e) {
			try {
				binFile.setLength(lengthBefore);
			} catch (IOException e1) {
				throw new OmniException(
						"\nAn error has occured while writing to table '"
								+ tableName
								+ "'.  This table may be corrupt!\n");
			}
			throw new OmniException(e.getMessage());
		}
	}

	public Dataset select(String whereClause) throws OmniException {
		WhereStatement parsedWhere = new WhereStatement(whereClause, fields);
		Dataset select = new Dataset(this);

		try {
			Iterator<Long> it = parsedWhere.iterator(binFile, rowSize);
			while (it.hasNext()) {
				binFile.seek(it.next() + 1);
				Row newRow = new Row();
				for (Field field : fields)
					newRow.add(field.read(binFile));
				select.add(newRow);
			}
			return select;
		} catch (IOException e) {
			if (Driver.debug) {
				e.printStackTrace();
				System.out.println("DEBUG = " + Driver.debug
						+ ", stack trace printed.");
			}
			throw new OmniException(
					"\nAn error has occured while reading table '" + tableName
							+ "'.  This table may be corrupt.\n");
		}
	}

	/**
	 * Renames a Table.
	 * 
	 * @param newName
	 *            The new name for a Table.
	 */
	public void setName(String newName) {
		tableName = newName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String table = tableName + ":\n";
		for (Field field : fields) {
			if (field.hasIndex())
				table += "* ";
			else
				table += "| ";
			table += field.toString() + "\n";
		}
		return table;
	}

	/**
	 * Generates an XML representation of the table.
	 * 
	 * @return XML representation of the table.
	 */
	public String toXMLString() {
		String xml = "\t<TABLE NAME=\"" + tableName + "\">\n";
		for (Field field : fields) {
			xml += field.toXMLString();
		}
		xml += "\t</TABLE>";
		return xml;
	}

	/**
	 * Updates rows of data in a table.
	 * 
	 * @param fieldName
	 *            Name of the field to be updated.
	 * @param newValue
	 *            New value for the field.
	 * @param whereClause
	 *            Rows are updated when this where clause evaluates true.
	 * @throws OmniException
	 *             Thrown if a table is not defined, a field is not defined, or
	 *             if there is an error reading or writing to the table files.
	 */
	@SuppressWarnings("rawtypes")
	public void update(String fieldName, String newValue, String whereClause)
			throws OmniException {
		WhereStatement parsedWhere = new WhereStatement(whereClause, fields);

		for (Field field : fields) {
			if (field.getName().equals(fieldName)) {
				try {
					Iterator<Long> it = parsedWhere.iterator(binFile, rowSize);
					while (it.hasNext()) {
						long rowPos = it.next();
						if (field.getIndex() != null) {
							binFile.seek(rowPos + field.getPos());
							Value oldValue = field.read(binFile);
							if (Driver.debug)
								System.out.println("Updating "
										+ oldValue.toString() + " from index "
										+ field.getName() + ".");
							field.updateIndex(oldValue,
									field.makeValue(newValue), rowPos, binFile);
						}
						binFile.seek(rowPos + field.getPos());
						field.write(binFile, newValue);
					}
					return;
				} catch (IOException e) {
					throw new OmniException(
							"\nAn error has occured while updating table '"
									+ tableName
									+ "'.  This table may be corrupt!\n");
				}
			}
		}
		throw new OmniException(fieldName + " is not defined in table "
				+ tableName + ".");
	}

	public void writeBin() throws IOException {
		binFile = new RandomAccessFile(DIRECTORY + File.separator
				+ this.tableName, "rw");
		this.data.write(this.binFile);
	}
}