package jdavis31.data;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.util.ArrayList;

import jdavis31.Driver;
import jdavis31.FileStructure;
import jdavis31.OmniException;
import jdavis31.data.fields.Field;
import jdavis31.data.values.Value;

public class BinarySearchTree implements FileStructure {

	protected RandomAccessFile indexFile;
	private Field field;

	public BinarySearchTree(String tableName, Field field,
			boolean replaceExisting) throws OmniException {
		try {
			if (replaceExisting)
				Files.deleteIfExists(new File(DIRECTORY + File.separator
						+ tableName + File.separator + field.getName())
						.toPath());
			this.indexFile = new RandomAccessFile(DIRECTORY + File.separator
					+ tableName + File.separator + field.getName(), "rw");
			this.field = field;
		} catch (IOException e) {
			if (Driver.debug) {
				e.printStackTrace();
				System.out.println("DEBUG = " + Driver.debug
						+ ", stack trace printed.");
			}
			throw new OmniException(
					"Error defining index for table '"
							+ tableName
							+ "'!  You may not have appropriate read/write permissions in this location.");
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void delete(Value newValue, BinaryNode root,
			RandomAccessFile tableData) throws OmniException {
		try {
			if (root.getRow(indexFile) == -1)
				throw new OmniException("Value " + newValue.toString()
						+ " not found.");

			if (Driver.debug)
				System.out.println("Checking row " + root.getRow(indexFile));

			tableData.seek(root.getRow(indexFile) + field.getPos());
			Value oldValue = field.read(tableData);
			int eval = newValue.compareTo(oldValue);

			if (eval == 0) {
				if (root.getLeftChild(indexFile).getRow(indexFile) != -1
						&& root.getRightChild(indexFile).getRow(indexFile) != -1) {
					BinaryNode successor = successor(root);
					root.setRow(successor.getRow(indexFile), indexFile);
					successor.delete(indexFile);
				} else {
					root.delete(indexFile);
				}
			} else if (eval < 0) {
				delete(newValue, root.getLeftChild(indexFile), tableData);
			} else {
				delete(newValue, root.getRightChild(indexFile), tableData);
			}
		} catch (IOException e) {
			throw new OmniException(e.getMessage());
		}
	}

	public void deleteBin(String tableName) throws IOException {
		indexFile.close();
		Files.deleteIfExists(new File(DIRECTORY + File.separator + tableName
				+ File.separator + field.getName()).toPath());
	}

	public BinaryNode getRoot() {
		return new BinaryNode(0, indexFile);
	}

	public ArrayList<Long> inOrderWalk(BinaryNode root) throws IOException,
			OmniException {
		ArrayList<Long> positions = new ArrayList<Long>();
		if (root.getRow(indexFile) != -1) {
			positions.addAll(inOrderWalk(root.getLeftChild(indexFile)));
			positions.add(root.getRow(indexFile));
			positions.addAll(inOrderWalk(root.getRightChild(indexFile)));
		}
		return positions;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BinaryNode insert(Value newValue, long newRowPos, BinaryNode root,
			RandomAccessFile tableData) throws OmniException {
		try {
			if (root.getRow(indexFile) == -1) {
				root.setRow(newRowPos, indexFile);
				return root;
			}
			tableData.seek(root.getRow(indexFile) + field.getPos());
			Value oldValue = field.read(tableData);

			if (newValue.compareTo(oldValue) < 0)
				insert(newValue, newRowPos, root.getLeftChild(indexFile),
						tableData);
			else
				insert(newValue, newRowPos, root.getRightChild(indexFile),
						tableData);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return root;
	}

	public BinaryNode max(BinaryNode root) throws IOException, OmniException {
		BinaryNode node = root.getRightChild(indexFile);
		if (node.getRow(indexFile) == -1)
			return root;
		return max(node);
	}

	public BinaryNode min(BinaryNode root) throws OmniException {
		BinaryNode node = root.getLeftChild(indexFile);
		if (node.getRow(indexFile) == -1)
			return root;
		return min(node);
	}

	public ArrayList<Long> reverseInOrderWalk(BinaryNode root)
			throws IOException, OmniException {
		ArrayList<Long> positions = new ArrayList<Long>();
		if (root.getRow(indexFile) != -1) {
			positions.addAll(reverseInOrderWalk(root.getRightChild(indexFile)));
			positions.add(root.getRow(indexFile));
			positions.addAll(reverseInOrderWalk(root.getLeftChild(indexFile)));
		}
		return positions;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BinaryNode select(Value newValue, BinaryNode root,
			RandomAccessFile tableData, boolean selectNearest)
			throws IOException, OmniException {

		if (root.getRow(indexFile) == -1) {
			if (selectNearest)
				return root.getParent(indexFile);
			else
				throw new OmniException("Value " + newValue.toString()
						+ " not found.");
		}

		if (Driver.debug)
			System.out.println("Checking row " + root.getRow(indexFile));

		tableData.seek(root.getRow(indexFile) + field.getPos());
		Value oldValue = field.read(tableData);
		int eval = newValue.compareTo(oldValue);
		if (eval == 0)
			return root;
		else if (eval < 1)
			return select(newValue, root.getLeftChild(indexFile), tableData,
					selectNearest);
		else
			return select(newValue, root.getRightChild(indexFile), tableData,
					selectNearest);
	}

	public BinaryNode successor(BinaryNode root) throws OmniException {
		return min(root.getRightChild(indexFile));
	}

	public String toString(RandomAccessFile tableData) throws IOException,
			OmniException {
		String tree = "\n[BinarySearchTree]\n";

		tableData.seek(min(getRoot()).getRow(indexFile) + field.getPos());
		tree += "\tmin()\t" + field.read(tableData).toString() + "\n";

		tableData.seek(max(getRoot()).getRow(indexFile) + field.getPos());
		tree += "\tmax()\t" + field.read(tableData).toString() + "\n";

		tableData.seek(successor(getRoot()).getRow(indexFile) + field.getPos());
		tree += "\tsuccessor()\t" + field.read(tableData).toString() + "\n";

		tree += "\tinOrderWalk()\n";
		ArrayList<Long> positions = inOrderWalk(getRoot());
		for (Long pos : positions) {
			tableData.seek(pos + field.getPos());
			tree += "\t\t" + field.read(tableData).toString() + "\n";
		}

		tree += "\treverseInOrderWalk()\n";
		positions = reverseInOrderWalk(getRoot());
		for (Long pos : positions) {
			tableData.seek(pos + field.getPos());
			tree += "\t\t" + field.read(tableData).toString() + "\n";
		}
		return tree;
	}
}
