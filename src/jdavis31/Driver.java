package jdavis31;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import jdavis31.data.xml.SAXReader;
import jdavis31.utilities.Utilities;

import org.xml.sax.SAXException;

/**
 * Main methods for reading and executing commands from user.
 * 
 * @author Joshua Davis
 */
public class Driver implements FileStructure {

	public static boolean debug = false;

	/**
	 * Main method of database program. Loads stored database and begins
	 * listening for user input.
	 * 
	 * @param args
	 *            Arguments for program execution.
	 */
	public static void main(String[] args) {
		try {
			if (args.length > 0 && args[0].equalsIgnoreCase("DEBUG"))
				debug = true;
			if (debug)
				System.out.println("DEBUG = " + debug);
			File fileStructure = new File(DIRECTORY);
			fileStructure.mkdirs();
			new SAXReader().saxReader(DIRECTORY + File.separator
					+ "database.xml");
		} catch (SAXException | IOException | OmniException e) {
			if (Driver.debug) {
				e.printStackTrace();
				System.out.println("DEBUG = " + Driver.debug
					+ ", stack trace printed.");
			} else
				System.out.println("Database failed to load: " + e.getMessage());
		}
		new Driver().run(new Scanner(System.in));
	}

	/**
	 * Reads scanner, concatenates incomplete commands, sends complete commands
	 * to command classes for syntax check, and executes syntactically correct
	 * commands.
	 * 
	 * @param sc
	 *            Scanner to be read for commands.
	 */
	public void run(Scanner sc) {
		String input = "";
		int index = 0;

		while (sc.hasNext()) {
			while (!input.contains(";"))
				input = input + " " + sc.nextLine();
			do {
				index = input.indexOf(';');
				Utilities.processInput(input.substring(0, index + 1).trim());
				input = input.substring(index + 1);
			} while (input.contains(";"));
		}
	}
}