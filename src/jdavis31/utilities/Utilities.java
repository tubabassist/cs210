package jdavis31.utilities;

import java.io.InputStream;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jdavis31.Driver;
import jdavis31.OmniException;
import jdavis31.commands.BackupCommand;
import jdavis31.commands.DefineIndexCommand;
import jdavis31.commands.DefineTableCommand;
import jdavis31.commands.DeleteCommand;
import jdavis31.commands.DropCommand;
import jdavis31.commands.ExitCommand;
import jdavis31.commands.HelpCommand;
import jdavis31.commands.ICommand;
import jdavis31.commands.InsertCommand;
import jdavis31.commands.PrintCommand;
import jdavis31.commands.ReadCommand;
import jdavis31.commands.ReadTableCommand;
import jdavis31.commands.RenameCommand;
import jdavis31.commands.RestoreCommand;
import jdavis31.commands.UpdateCommand;
import jdavis31.commands.queries.IQuery;
import jdavis31.commands.queries.IntersectionCommand;
import jdavis31.commands.queries.JoinCommand;
import jdavis31.commands.queries.MinusCommand;
import jdavis31.commands.queries.OrderCommand;
import jdavis31.commands.queries.ProjectionCommand;
import jdavis31.commands.queries.SelectCommand;
import jdavis31.commands.queries.UnionCommand;
import jdavis31.data.Dataset;

public class Utilities {

	private static final IQuery[] QUERY_COMMANDS = new IQuery[] {
			new SelectCommand(), new ProjectionCommand(), new JoinCommand(),
			new IntersectionCommand(), new UnionCommand(), new MinusCommand(),
			new OrderCommand(), };

	private static final ICommand[] COMMANDS = new ICommand[] {
			new PrintCommand(), new ExitCommand(), new ReadCommand(),
			new BackupCommand(), new RestoreCommand(),
			new DefineTableCommand(), new RenameCommand(), new DropCommand(),
			new DefineIndexCommand(), new DeleteCommand(), new InsertCommand(),
			new UpdateCommand(), new HelpCommand(), new ReadTableCommand() };

	public static String formatOutput(Dataset tableData) {
		String formattedTable = "\n";
		formattedTable += tableData.toString();
		formattedTable += "\n<-- End of Selection -->\n";
		return formattedTable;
	}

	public static String formatOutput(String tableName, Dataset tableData) {
		String formattedTable = "";
		formattedTable = "\n" + tableName + ":\n";
		formattedTable += tableData.toString();
		formattedTable += "\n<-- End of Selection -->\n";
		return formattedTable;
	}

	public static void processInput(String input) {
		try {
			for (IQuery query : QUERY_COMMANDS) {
				if (query.matches(input)) {
					query.execute();
					return;
				}
			}
			for (ICommand command : COMMANDS) {
				if (command.matches(input)) {
					try {
						command.execute();
					} catch (OmniException e) {
						if (Driver.debug) {
							e.printStackTrace();
							System.out.println("DEBUG = " + Driver.debug
									+ ", stack trace printed.");
						} else
							System.err.println(e.getMessage());
					}
					return;
				}
			}
		} catch (OmniException e) {
			System.err.println(e.getMessage());
			return;
		}
		System.err
				.println("Invalid statement!  Type 'help;' for proper syntax.");
	}

	public static Dataset processQueryInput(String input) throws OmniException {
		for (IQuery query : QUERY_COMMANDS) {
			if (query.matches(input))
				return query.executeQuery();
		}
		throw new OmniException(
				"Invalid command!  Check syntax in your query lists.");
	}

	public static void printHelp() {
		InputStream input = Utilities.class.getResourceAsStream("grammar.txt");
		Scanner sc = new Scanner(input);
		while (sc.hasNextLine()) {
			System.out.println(sc.nextLine());
		}
		sc.close();
	}

	public static String removeQuotes(String value) throws OmniException {
		Pattern pattern = Pattern.compile(".*('|\")(.*)('|\").*");
		Matcher matcher = pattern.matcher(value);
		if (matcher.matches()) {
			return matcher.group(2);
		} else {
			return value;
		}
	}
}
