package jdavis31.data.values;

import static org.junit.Assert.*;

import org.junit.Test;

public abstract class AbstractValueTest {
	@SuppressWarnings("rawtypes")
	protected Value value1;
	@SuppressWarnings("rawtypes")
	protected Value value2;
	@SuppressWarnings("rawtypes")
	protected Value value3;

	@SuppressWarnings("unchecked")
	@Test
	public void testCompareTo() {
		assertTrue(value1.compareTo(value2) < 0);
		assertTrue(value2.compareTo(value1) > 0);
		assertTrue(value3.compareTo(value3) == 0);
	}
}
