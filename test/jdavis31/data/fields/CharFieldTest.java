package jdavis31.data.fields;

import jdavis31.data.fields.CharField;

import org.junit.Before;

public class CharFieldTest extends AbstractFieldTest {

	@Override
	@Before
	public void setUp() throws Exception {
		field = new CharField(goodName, 0, 2);
		goodType = "char";
		badTypes = new String[] { "boolean", "date", "integer", "real",
				"varchar" };
	}
}