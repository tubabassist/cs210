package jdavis31.data.fields;

import static org.junit.Assert.*;
import jdavis31.data.fields.Field;

import org.junit.Test;

public abstract class AbstractFieldTest {
	
	protected Field field;
	
	public abstract void setUp() throws Exception;
	
	protected String goodName = "testName";
	protected String[] badNames = new String[] { "test", "tests", "tester" };
	protected String goodType;
	protected String[] badTypes;
	
	public AbstractFieldTest() {
		super();
	}

	@Test
	public void testGetName() {
		assertTrue(field.getName().matches(goodName));
		for (String s : badNames) {
			assertFalse(field.getName().matches(s));
		}
	}

	@Test
	public void testGetType() {
		assertTrue(field.getType().matches(goodType));
		for (String s : badTypes) {
			assertFalse(field.getType().matches(s));
		}
	}
}
