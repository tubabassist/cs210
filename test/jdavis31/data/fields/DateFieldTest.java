package jdavis31.data.fields;

import jdavis31.data.fields.DateField;

import org.junit.Before;

public class DateFieldTest extends AbstractFieldTest {

	@Override
	@Before
	public void setUp() throws Exception {
		field = new DateField(goodName, 0);
		goodType = "date";
		badTypes = new String[] { "char(\\d+)", "boolean", "integer", "real",
				"varchar" };
	}
}