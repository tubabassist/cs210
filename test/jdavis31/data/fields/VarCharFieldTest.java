package jdavis31.data.fields;

import jdavis31.data.fields.VarCharField;

import org.junit.Before;

public class VarCharFieldTest extends AbstractFieldTest {

	@Override
	@Before
	public void setUp() throws Exception {
		field = new VarCharField(goodName, 0);
		goodType = "varchar";
		badTypes = new String[] { "char(\\d+)", "date", "integer", "real",
				"boolean" };
	}
}