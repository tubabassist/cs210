package jdavis31.data.fields;

import jdavis31.data.fields.IntegerField;

import org.junit.Before;

public class IntegerFieldTest extends AbstractFieldTest {

	@Override
	@Before
	public void setUp() throws Exception {
		field = new IntegerField(goodName, 0);
		goodType = "integer";
		badTypes = new String[] { "char(\\d+)", "date", "boolean", "real",
				"varchar" };
	}
}