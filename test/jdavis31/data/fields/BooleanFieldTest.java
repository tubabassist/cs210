package jdavis31.data.fields;

import jdavis31.data.fields.BooleanField;

import org.junit.Before;

public class BooleanFieldTest extends AbstractFieldTest {

	@Override
	@Before
	public void setUp() throws Exception {
		field = new BooleanField(goodName, 0);
		goodType = "boolean";
		badTypes = new String[] { "char(\\d+)", "date", "integer", "real",
				"varchar" };
	}
}