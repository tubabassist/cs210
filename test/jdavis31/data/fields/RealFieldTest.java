package jdavis31.data.fields;

import jdavis31.data.fields.RealField;

import org.junit.Before;

public class RealFieldTest extends AbstractFieldTest {

	@Override
	@Before
	public void setUp() throws Exception {
		field = new RealField(goodName, 0);
		goodType = "real";
		badTypes = new String[] { "char(\\d+)", "date", "integer", "boolean",
				"varchar" };
	}
}