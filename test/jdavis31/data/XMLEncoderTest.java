package jdavis31.data;

import static org.junit.Assert.*;
import jdavis31.data.xml.XMLEncoder;

import org.junit.Before;
import org.junit.Test;

public class XMLEncoderTest {
	private String[] input;

	@Before
	public void setUp() throws Exception {
		input = new String[] {"te\"st", "te&st", "te'st", "te<st", "te>st"};
	}

	@Test
	public void testEncode() {
		//assertTrue(XMLEncoder.encode(input[0]).equals("te&quot;st"));
		assertTrue(XMLEncoder.encode(input[1]).equals("te&amp;st"));
		assertTrue(XMLEncoder.encode(input[2]).equals("te&apos;st"));
		assertTrue(XMLEncoder.encode(input[3]).equals("te&lt;st"));
		assertTrue(XMLEncoder.encode(input[4]).equals("te&gt;st"));
		assertFalse(XMLEncoder.encode(input[0]).equals("te\"st"));
		assertFalse(XMLEncoder.encode(input[1]).equals("te&st"));
		assertFalse(XMLEncoder.encode(input[2]).equals("te'st"));
		assertFalse(XMLEncoder.encode(input[3]).equals("te<st"));
		assertFalse(XMLEncoder.encode(input[4]).equals("te>st"));
	}

}
