package jdavis31.data;

import static org.junit.Assert.*;
import jdavis31.data.Table;

import org.junit.Before;
import org.junit.Test;

public class TableTest {
	Table table;
	Table good;

	@Before
	public void setUp() throws Exception {
		table = new Table("Test", "id char(2), name varchar, currentDate date, employed boolean");
		good = new Table("Test", "id char(2), name varchar, currentDate date, employed boolean");
	}

	@Test
	public void testGetName() {
		assertTrue(table.getName().equals("TEST"));
	}

	@Test
	public void testSetName() {
		table.setName("New");
		assertTrue(table.getName().equals("New"));
		assertFalse(table.getName().equals("Test"));
	}
}