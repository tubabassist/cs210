package jdavis31.data;

import jdavis31.OmniException;

import org.junit.Before;
import org.junit.Test;

public class TableCollectionTest {

	@Before
	public void setUp() throws Exception {
		TableCollection.getTC();
	}

	@Test
	public void testGetTC() {
		TableCollection.getTC();
	}

	@Test
	public void testPut() throws OmniException {
		TableCollection.getTC()
				.put(new Table("test",
						"test integer, tests varchar, tester boolean"));
	}

	@Test
	public void testRemove() throws OmniException {
		TableCollection.getTC().remove("test");
	}

	@Test
	public void testRename() throws OmniException {
		TableCollection.getTC()
		.put(new Table("test",
				"test integer, tests varchar, tester boolean"));
		TableCollection.getTC().rename("test", "test2");
	}

	@Test
	public void testToString() {
		TableCollection.getTC().toString();
	}

	@Test
	public void testWriteToFile() throws OmniException {
		//TableCollection.getTC().writeToFile();
	}
	
	@Test
	public void testInsertAndPrint() throws OmniException {
		TableCollection.getTC().insert("test", "9000, If you can read this then the test worked!, true");
	}

}
