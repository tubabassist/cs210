package jdavis31.data;

import java.io.IOException;

import jdavis31.OmniException;
import jdavis31.data.xml.SAXReader;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

public class SAXReaderTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSaxReader() throws SAXException, IOException, OmniException {
		new SAXReader().saxReader("database.xml");
	}

}
