package jdavis31.commands;

import jdavis31.commands.DefineTableCommand;

import org.junit.Before;

public class DefineTableCommandTest extends AbstractCommandTest {

	@Override
	@Before
	public void setUp() throws Exception {
		command = new DefineTableCommand();
		good = new String[] {
				"define table emp having fields(name varchar, age integer, state char(2));",
				"define table emp having fields (name varchar, age integer, state char(2) );" };
		bad = new String[] { "define;", "define table;", "define table emp;",
				"define table emp having;", "define table emp having fields;",
				"define table emp having fields name varchar, age integer, state char(2);" };
	}
}