package jdavis31.commands;

import jdavis31.commands.queries.MinusCommand;

import org.junit.Before;

public class MinusCommandTest extends AbstractCommandTest {

	@Override
	@Before
	public void setUp() throws Exception {
		command = new MinusCommand();
		good = new String[] { "minus emp and dept;" };
		bad = new String[] { "minus;", "minus emp dept;",
				"minus emp and dept and pos;" };
	}
}