package jdavis31.commands;

import jdavis31.commands.BackupCommand;

import org.junit.Before;

public class BackupCommandTest extends AbstractCommandTest {

	@Override
	@Before
	public void setUp() throws Exception {
		command = new BackupCommand();
		good = new String[] { "backup to '/home/dufus/blech';" };
		bad = new String[] { "backup;", "backup to;",
				"backup to /home/dufus/blech;" };
	}
}