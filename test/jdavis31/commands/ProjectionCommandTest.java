package jdavis31.commands;

import jdavis31.commands.queries.ProjectionCommand;

import org.junit.Before;

public class ProjectionCommandTest extends AbstractCommandTest {

	@Override
	@Before
	public void setUp() throws Exception {
		command = new ProjectionCommand();
		good = new String[] { "project emp over a, b, c;" };
		bad = new String[] { "project;", "project emp;" };
	}
}