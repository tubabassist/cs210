package jdavis31.commands;

import jdavis31.commands.PrintCommand;

import org.junit.Before;

public class PrintCommandTest extends AbstractCommandTest {

	@Override
	@Before
	public void setUp() throws Exception {
		command = new PrintCommand();
		good = new String[] { "print emp;", "print dictionary;" };
		bad = new String[] { "print;" };
	}
}