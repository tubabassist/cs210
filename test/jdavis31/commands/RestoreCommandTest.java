package jdavis31.commands;

import jdavis31.commands.RestoreCommand;

import org.junit.Before;

public class RestoreCommandTest extends AbstractCommandTest {

	@Override
	@Before
	public void setUp() throws Exception {
		command = new RestoreCommand();
		good = new String[] { "restore from 'fred';" };
		bad = new String[] { "restore;", "restore from fred;",
				"restore 'fred';" };
	}
}