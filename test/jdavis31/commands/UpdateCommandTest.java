package jdavis31.commands;

import jdavis31.commands.UpdateCommand;

import org.junit.Before;

public class UpdateCommandTest extends AbstractCommandTest {

	@Override
	@Before
	public void setUp() throws Exception {
		command = new UpdateCommand();
		good = new String[] { "update emp set sal = 200 where name = 'jones';",
				"update emp set sal = 200;" };
		bad = new String[] { "update;", "update emp;", "update emp set sal;" };
	}
}