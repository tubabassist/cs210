package jdavis31.commands;

import jdavis31.commands.queries.UnionCommand;

import org.junit.Before;

public class UnionCommandTest extends AbstractCommandTest {

	@Override
	@Before
	public void setUp() throws Exception {
		command = new UnionCommand();
		good = new String[] { "union emp and dept;" };
		bad = new String[] { "union;", "union emp dept;", "union emp;" };
	}
}