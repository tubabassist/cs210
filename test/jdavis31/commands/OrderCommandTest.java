package jdavis31.commands;

import jdavis31.commands.queries.OrderCommand;

import org.junit.Before;

public class OrderCommandTest extends AbstractCommandTest {

	@Override
	@Before
	public void setUp() throws Exception {
		command = new OrderCommand();
		good = new String[] { "order emp by sal;" };
		bad = new String[] { "order;", "order emp sal;",
				"order emp by sal and pos;" };
	}
}