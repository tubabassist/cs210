package jdavis31.commands;

import jdavis31.commands.DeleteCommand;

import org.junit.Before;

public class DeleteCommandTest extends AbstractCommandTest {

	@Override
	@Before
	public void setUp() throws Exception {
		command = new DeleteCommand();
		good = new String[] { "delete emp where sal < 100;", "delete emp;" };
		bad = new String[] { "delete;" };
	}
}