package jdavis31.commands;

import jdavis31.commands.queries.IntersectionCommand;

import org.junit.Before;

public class IntersectionCommandTest extends AbstractCommandTest {

	@Override
	@Before
	public void setUp() throws Exception {
		command = new IntersectionCommand();
		good = new String[] { "intersect emp and dept;" };
		bad = new String[] { "intersect;", "intersect emp;", "intersect and;",
				"intersect emp dept;", "intersect emp and dept and pos;" };
	}
}