package jdavis31.commands;

import jdavis31.commands.InsertCommand;

import org.junit.Before;

public class InsertCommandTest extends AbstractCommandTest {

	@Override
	@Before
	public void setUp() throws Exception {
		command = new InsertCommand();
		good = new String[] { "insert (1, 'hello', 33.1, '02/19/2014') into emp;" };
		bad = new String[] { "insert;", "insert into;", "insert into emp;" };
	}
}