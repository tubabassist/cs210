package jdavis31.commands;

import jdavis31.commands.ExitCommand;

import org.junit.Before;

public class ExitCommandTest extends AbstractCommandTest {

	@Override
	@Before
	public void setUp() throws Exception {
		command = new ExitCommand();
		good = new String[] { "exit;" };
		bad = new String[] { "exit", "select emp x < 100;" };
	}
}