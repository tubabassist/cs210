package jdavis31.commands;

import jdavis31.commands.RenameCommand;

import org.junit.Before;

public class RenameCommandTest extends AbstractCommandTest {

	@Override
	@Before
	public void setUp() throws Exception {
		command = new RenameCommand();
		good = new String[] { "rename table fred to jones;" };
		bad = new String[] { "rename;", "rename fred to jones;",
				"rename table to jones;" };
	}
}