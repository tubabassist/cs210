package jdavis31.commands;

import jdavis31.commands.queries.SelectCommand;

import org.junit.Before;

public class SelectCommandTest extends AbstractCommandTest {

	@Override
	@Before
	public void setUp() throws Exception {
		command = new SelectCommand();
		good = new String[] { "select emp;", "select emp where x < 100;",
				"SELECT EMP;" };
		bad = new String[] { "selectemp;", "select emp x < 100;" };
	}
}