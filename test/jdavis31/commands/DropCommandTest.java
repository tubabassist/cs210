package jdavis31.commands;

import jdavis31.commands.DropCommand;

import org.junit.Before;

public class DropCommandTest extends AbstractCommandTest {

	@Override
	@Before
	public void setUp() throws Exception {
		command = new DropCommand();
		good = new String[] { "drop table Dave;" };
		bad = new String[] { "drop;", "drop table;" };
	}
}