package jdavis31.commands;

import jdavis31.commands.ReadCommand;

import org.junit.Before;

public class ReadCommandTest extends AbstractCommandTest {

	@Override
	@Before
	public void setUp() throws Exception {
		command = new ReadCommand();
		good = new String[] { "read 'C:/Program Files/myfile';" };
		bad = new String[] { "read;", "read C:/Program Files/myfile;" };
	}
}