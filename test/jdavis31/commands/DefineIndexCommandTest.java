package jdavis31.commands;

import jdavis31.commands.DefineIndexCommand;

import org.junit.Before;

public class DefineIndexCommandTest extends AbstractCommandTest {

	@Override
	@Before
	public void setUp() throws Exception {
		command = new DefineIndexCommand();
		good = new String[] { "define index on emp(sal);",
				"define index on emp (sal);", "define index on emp(sal) ;",
				"define index on emp (sal) ;", "define index on emp(sal, pos);" };
		bad = new String[] { "define;", "define index;", "define index on;",
				"define index on emp;", "define index on emp sal;",
				"defineindex on emp(sal);" };
	}
}