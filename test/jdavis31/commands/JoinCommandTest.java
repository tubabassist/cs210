package jdavis31.commands;

import jdavis31.commands.queries.JoinCommand;

import org.junit.Before;

public class JoinCommandTest extends AbstractCommandTest {

	@Override
	@Before
	public void setUp() throws Exception {
		command = new JoinCommand();
		good = new String[] { "join emp and dept;" };
		bad = new String[] { "join;", "join emp dept;",
				"join emp and dept and pos;" };
	}
}